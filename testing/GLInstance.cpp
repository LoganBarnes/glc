// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include "GLInstance.hpp"
#include <GLFW/glfw3.h>
#include <iostream>

// Can uncomment this to debug if something goes wrong.
// Otherwise it throws off code coverage calculations.
//#define CHECK_ERRORS

namespace glc {

namespace testing {

#ifdef CHECK_ERRORS
#ifndef __APPLE__
namespace {

void opengl_callback(GLenum /*source*/,
                     GLenum type,
                     GLuint /*id*/,
                     GLenum severity,
                     GLsizei /*length*/,
                     const GLchar *message,
                     const void * /*userParam*/)
{
    if (type == GL_DEBUG_TYPE_ERROR) {
        fprintf(stderr,
                "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
                "** GL ERROR **",
                type,
                severity,
                message);
    }
}

} // namespace
#endif
#endif

GLInstance::GLInstance()
{
#ifdef CHECK_ERRORS
    // Set the error callback before any other GLFW calls so we get proper error reporting
    glfwSetErrorCallback([](int error, const char *description) {
        std::cerr << "ERROR: (" << error << ") " << description << std::endl;
    });

    auto init_err_stat =
#endif
        glfwInit();

#ifdef CHECK_ERRORS
    if (init_err_stat == 0) {
        throw std::runtime_error("GLFW init failed");
    }
#endif

    // OpenGL 4.3 required for SSBO
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
#ifdef __APPLE__
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
#else
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
#endif // __APPLE__
    int width = 640;
    int height = 480;

    window_ = glfwCreateWindow(width, height, "", nullptr, nullptr);

#ifdef CHECK_ERRORS
    if (window_ == nullptr) {
        glfwTerminate();
        throw std::runtime_error("GLFW window creation failed");
    }
#endif

    glfwMakeContextCurrent(window_);
    glfwSwapInterval(1);

#ifdef CHECK_ERRORS
    auto glad_err_stat =
#endif
        gl3wInit();

#ifdef CHECK_ERRORS
    if (glad_err_stat) {
        glfwDestroyWindow(window_);
        glfwTerminate();
        throw std::runtime_error("Failed load OpenGL Glad functions");
    }

#ifndef __APPLE__
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(reinterpret_cast<GLDEBUGPROC>(opengl_callback), nullptr);
#endif
#endif
}

GLInstance::~GLInstance()
{
    glfwDestroyWindow(window_);
    glfwTerminate();
}

} // namespace testing

} // namespace glc
