// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include "TestUtils.hpp"
#include <ostream>

namespace glc {
namespace testing {

StreamRedirectGuard::StreamRedirectGuard(std::streambuf *new_buffer, std::ostream &os)
    : ostream_(os), old_buf_(ostream_.rdbuf(new_buffer))
{}

StreamRedirectGuard::~StreamRedirectGuard()
{
    ostream_.rdbuf(old_buf_);
}

} // namespace testing
} // namespace glc
