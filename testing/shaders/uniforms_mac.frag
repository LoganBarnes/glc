// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#version 410
#extension GL_ARB_separate_shader_objects : enable

uniform bool buniform;

uniform int iuniform;
uniform ivec2 i2uniform;
uniform ivec3 i3uniform;
uniform ivec4 i4uniform;

uniform uint uuniform;
uniform uvec2 u2uniform;
uniform uvec3 u3uniform;
uniform uvec4 u4uniform;

uniform float funiform;
uniform vec2 f2uniform;
uniform vec3 f3uniform;
uniform vec4 f4uniform;

uniform mat2 m2uniform;
uniform mat3 m3uniform;
uniform mat4 m4uniform;

uniform sampler2D tex;

layout(location = 0) out vec4 out_color;

void main()
{
    vec4 color;

    if (buniform) {
        color = vec4(0.1, 0.2, 0.3, 0.4);
    } else {
        color = texture(tex, f2uniform);
    }

    color.x += iuniform;
    color.xy += i2uniform;
    color.xyz += i3uniform;
    color.xyzw += i4uniform;

    color.x -= iuniform;
    color.xy -= i2uniform;
    color.xyz -= i3uniform;
    color.xyzw -= i4uniform;

    color.r *= funiform;
    color.rg *= f2uniform;
    color.rgb *= f3uniform;
    color.rgba *= f4uniform;

    color.st = m2uniform * color.st;
    color.stp = m3uniform * color.stp;
    color.stpq = m4uniform * color.stpq;

	out_color = color;
}
