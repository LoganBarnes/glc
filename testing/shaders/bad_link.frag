// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#version 410
#extension GL_ARB_separate_shader_objects : enable

in vec3 color;

layout(location = 0) out vec4 out_color;

void main()
{
	out_color = vec4(color, 1.0);
}
