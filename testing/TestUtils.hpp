// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <streambuf>

namespace glc {
namespace testing {

struct StreamRedirectGuard
{

    StreamRedirectGuard(std::streambuf *new_buffer, std::ostream &os);
    ~StreamRedirectGuard();

private:
    std::ostream &ostream_;
    std::streambuf *old_buf_;
};

} // namespace testing
} // namespace glc
