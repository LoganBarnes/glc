// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/SeparableProgram.hpp>
#include <glc/Texture.hpp>
#include "GLInstance.hpp"
#include <testing/ShaderConfig.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <gmock/gmock.h>

namespace {

class SeparableProgramTests : public ::testing::Test
{
    glc::testing::GLInstance gl_instance_;
};

#ifdef __APPLE__
const char *uniforms_frag = "uniforms_mac.frag";
#else
const char *uniforms_frag = "uniforms.frag";
#endif

TEST_F(SeparableProgramTests, SameShadersReturnSameProgram)
{
    glc::SeparableProgram program1 = glc::create_separable_program(
        {glc::testing::shader_path() + "shader1.vert", glc::testing::shader_path() + "shader1.frag"});

    glc::SeparableProgram program2 = glc::create_separable_program(
        {glc::testing::shader_path() + "shader1.vert", glc::testing::shader_path() + "shader2.frag"});

    glc::SeparableProgram program3 = glc::create_separable_program(
        {glc::testing::shader_path() + "shader1.vert", glc::testing::shader_path() + "shader1.frag"});

    glc::SeparableProgram program4 = glc::create_separable_program(
        {glc::testing::shader_path() + "shader1.vert", glc::testing::shader_path() + "shader2.frag"});

    EXPECT_EQ(program1->vert_program(), program2->vert_program());
    EXPECT_EQ(program2->vert_program(), program3->vert_program());
    EXPECT_EQ(program3->vert_program(), program4->vert_program());

    EXPECT_EQ(program1->frag_program(), program3->frag_program());
    EXPECT_EQ(program2->frag_program(), program4->frag_program());

    EXPECT_NE(program1->frag_program(), program2->frag_program());
    EXPECT_NE(program3->frag_program(), program4->frag_program());

    EXPECT_EQ(program1->vert_program()->get_id(), program2->vert_program()->get_id());
    EXPECT_EQ(program2->vert_program()->get_id(), program3->vert_program()->get_id());
    EXPECT_EQ(program3->vert_program()->get_id(), program4->vert_program()->get_id());

    EXPECT_EQ(program1->frag_program()->get_id(), program3->frag_program()->get_id());
    EXPECT_EQ(program2->frag_program()->get_id(), program4->frag_program()->get_id());

    EXPECT_NE(program1->frag_program()->get_id(), program2->frag_program()->get_id());
    EXPECT_NE(program3->frag_program()->get_id(), program4->frag_program()->get_id());
}

TEST_F(SeparableProgramTests, ProgramIsDeletedOutOfScope)
{
    GLuint orig_id;
    {
        glc::SeparableProgram program1 = glc::create_separable_program(
            {glc::testing::shader_path() + "shader1.vert", glc::testing::shader_path() + "shader1.frag"});

        orig_id = program1->frag_program()->get_id();
    }
    // no more shared pointers for program1. Next program with same shaders should be different.

    // create another program with different shaders just to make sure the previous id isn't used again
    glc::SeparableProgram other_program = glc::create_separable_program(
        {glc::testing::shader_path() + "shader1.vert", glc::testing::shader_path() + "shader2.frag"});

    // same shaders as program1 but the object should be different now
    glc::SeparableProgram program2 = glc::create_separable_program(
        {glc::testing::shader_path() + "shader1.vert", glc::testing::shader_path() + "shader1.frag"});

    EXPECT_NE(orig_id, program2->frag_program()->get_id());
}

TEST_F(SeparableProgramTests, ThrowOnBadFilename)
{
    EXPECT_THROW(glc::create_separable_program("not_a_file.vert"), std::exception);
}

TEST_F(SeparableProgramTests, ThrowOnBadCompilation)
{
    EXPECT_THROW(glc::create_separable_program(glc::testing::shader_path() + "invalid.vert"), std::exception);
}

TEST_F(SeparableProgramTests, ThrowOnBadExtension)
{
    EXPECT_THROW(glc::create_separable_program(glc::testing::shader_path() + "bad_extension.glsl"), std::exception);
}

TEST_F(SeparableProgramTests, BoolUniform)
{
    auto program = glc::create_separable_program(glc::testing::shader_path() + "shader1.vert",
                                                 glc::testing::shader_path() + uniforms_frag);

    program->use([&] {
        EXPECT_FALSE(program->frag_program()->set_uniform("wrong_name", true));

        EXPECT_TRUE(program->frag_program()->set_uniform("buniform", true));
    });
}

TEST_F(SeparableProgramTests, IntUniform)
{
    auto program = glc::create_separable_program(glc::testing::shader_path() + "shader1.vert",
                                                 glc::testing::shader_path() + uniforms_frag);

    program->use([&] {
        glm::ivec4 uniform = {1, 3, 5, 7};
        EXPECT_FALSE(program->frag_program()->set_int_uniform_array("wrong_name", glm::value_ptr(uniform)));

        EXPECT_TRUE(program->frag_program()->set_uniform("iuniform", uniform.x));
        EXPECT_TRUE(program->frag_program()->set_uniform("i2uniform", glm::ivec2(uniform)));
        EXPECT_TRUE(program->frag_program()->set_uniform("i3uniform", glm::ivec3(uniform)));
        EXPECT_TRUE(program->frag_program()->set_uniform("i4uniform", uniform));

        EXPECT_THROW(program->frag_program()->set_int_uniform_array("iuniform", glm::value_ptr(uniform), 0),
                     std::exception);
        EXPECT_THROW(program->frag_program()->set_int_uniform_array("iuniform", glm::value_ptr(uniform), 5),
                     std::exception);
    });
}

TEST_F(SeparableProgramTests, FloatUniform)
{
    auto program = glc::create_separable_program(glc::testing::shader_path() + "shader1.vert",
                                                 glc::testing::shader_path() + uniforms_frag);

    program->use([&] {
        glm::vec4 uniform = {0.1f, 0.3f, 0.5f, 0.7f};
        EXPECT_FALSE(program->frag_program()->set_float_uniform_array("wrong_name", glm::value_ptr(uniform)));

        EXPECT_TRUE(program->frag_program()->set_uniform("funiform", uniform.x));
        EXPECT_TRUE(program->frag_program()->set_uniform("f2uniform", glm::vec2(uniform)));
        EXPECT_TRUE(program->frag_program()->set_uniform("f3uniform", glm::vec3(uniform)));
        EXPECT_TRUE(program->frag_program()->set_uniform("f4uniform", uniform));

        EXPECT_THROW(program->frag_program()->set_float_uniform_array("funiform", glm::value_ptr(uniform), 0),
                     std::exception);
        EXPECT_THROW(program->frag_program()->set_float_uniform_array("funiform", glm::value_ptr(uniform), 5),
                     std::exception);
    });
}

TEST_F(SeparableProgramTests, MatrixUniform)
{
    auto program = glc::create_separable_program(glc::testing::shader_path() + "shader1.vert",
                                                 glc::testing::shader_path() + uniforms_frag);

    program->use([&] {
        glm::mat4 uniform(1);
        EXPECT_FALSE(program->frag_program()->set_matrix_uniform_array("wrong_name", glm::value_ptr(uniform)));

        EXPECT_TRUE(program->frag_program()->set_uniform("m2uniform", glm::mat2(1.f)));
        EXPECT_TRUE(program->frag_program()->set_uniform("m3uniform", glm::mat3(1.f)));
        EXPECT_TRUE(program->frag_program()->set_uniform("m4uniform", glm::mat4(1.f)));

        EXPECT_THROW(program->frag_program()->set_matrix_uniform_array("muniform", glm::value_ptr(uniform), 1),
                     std::exception);
        EXPECT_THROW(program->frag_program()->set_matrix_uniform_array("muniform", glm::value_ptr(uniform), 5),
                     std::exception);
    });
}

TEST_F(SeparableProgramTests, TextureUniform)
{
    auto program = glc::create_separable_program(glc::testing::shader_path() + "shader1.vert",
                                                 glc::testing::shader_path() + uniforms_frag);

    auto tex = glc::create_texture(128, 128);

    program->use([&] {
        EXPECT_FALSE(program->frag_program()->set_uniform("wrong_name", tex));

        EXPECT_TRUE(program->frag_program()->set_uniform("tex", tex));
    });
}

#ifndef __APPLE__
TEST_F(SeparableProgramTests, SSBOUniform)
{
    auto program = glc::create_separable_program(glc::testing::shader_path() + "shader1.vert",
                                                 glc::testing::shader_path() + uniforms_frag);

    std::vector<float> ssbo_data = {1, 2, 3, 4, 5, 6, 7};
    auto ssbo = glc::create_buffer<GL_SHADER_STORAGE_BUFFER>(ssbo_data);

    program->use([&] {
        EXPECT_FALSE(program->frag_program()->set_ssbo_uniform("wrong_name", ssbo, 0));

        EXPECT_TRUE(program->frag_program()->set_ssbo_uniform("ssboUniform", ssbo, 0));
    });
}
#endif

} // namespace
