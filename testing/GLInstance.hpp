// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <GL/gl3w.h>

struct GLFWwindow;

namespace glc {

namespace testing {

class GLInstance
{
public:
    GLInstance();
    ~GLInstance();

private:
    GLFWwindow *window_;
};

} // namespace testing

} // namespace glc
