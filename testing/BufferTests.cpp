// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/Buffer.hpp>
#include "GLInstance.hpp"
#include <gmock/gmock.h>
#include <chrono>
#include <random>

namespace {

class BufferTests : public ::testing::Test
{
    glc::testing::GLInstance gl_instance_;
};

TEST_F(BufferTests, BufferIsArrayByDefault)
{
    glc::Buffer<float> buffer = glc::create_buffer(std::vector<float>{});
    EXPECT_EQ(buffer->get_num_elements(), 0);

    EXPECT_EQ(buffer->get_buffer_type(), GL_ARRAY_BUFFER);
}

TEST_F(BufferTests, GetDataReturnsStoredData)
{
    const auto seed = std::chrono::steady_clock::now().time_since_epoch().count();

    std::vector<float> expected(1000000);
    {
        std::mt19937 gen(seed);
        std::uniform_real_distribution<float> dist;
        std::generate(expected.begin(), expected.end(), [&] { return dist(gen); });
    }

    auto buffer = glc::create_buffer(expected);
    EXPECT_EQ(buffer->get_num_elements(), expected.size());

    std::vector<float> actual;
    buffer->get_data(&actual);

    EXPECT_EQ(expected, actual) << "Seed: " << seed;
}

TEST_F(BufferTests, BufferDataCanBeUpdated)
{
    auto buffer = glc::create_buffer(std::vector<float>{0, 1, 2, 3, 4, 5, 6, 7});
    EXPECT_EQ(buffer->get_num_elements(), 8);

    // update values starting at index 2
    buffer->update(2, std::vector<float>{5, 4, 3, 2});
    EXPECT_EQ(buffer->get_num_elements(), 8);

    std::vector<float> expected{0, 1, 5, 4, 3, 2, 6, 7};

    std::vector<float> actual;
    buffer->get_data(&actual);

    EXPECT_EQ(expected, actual);
}

TEST_F(BufferTests, BufferDataCanBeResized)
{
    auto buffer = glc::create_buffer(std::vector<float>{0, 1, 2, 3, 4, 5, 6, 7});
    EXPECT_EQ(buffer->get_num_elements(), 8);

    // update values starting at index 2
    buffer->resize(std::vector<float>{5, 4, 3, 2});
    EXPECT_EQ(buffer->get_num_elements(), 4);

    std::vector<float> expected{5, 4, 3, 2};

    std::vector<float> actual;
    buffer->get_data(&actual);

    EXPECT_EQ(expected, actual);
}

} // namespace
