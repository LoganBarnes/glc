// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/scene/StaticScene.hpp>
#include <glc/camera/Camera.hpp>
#include "TestUtils.hpp"
#include "GLInstance.hpp"
#include <gmock/gmock.h>

class RenderableTests : public ::testing::Test
{
    glc::testing::GLInstance gl_instance_;
};

TEST_F(RenderableTests, test_on_render_gets_called)
{
    glc::StaticScene scene;
    glc::Camera camera;
    static bool function_run = false; // static global since on_render is const

    class RenderableTest : public glc::RenderableInterface
    {
    public:
        void on_render(const glc::Camera &,
                       const glc::UpdatableInfo &,
                       const glc::StaticScene &,
                       const glm::mat4 &) const final
        {
            function_run = true;
        }
    };

    RenderableTest renderable;
    scene.add_item(glc::Renderable(renderable));
    scene.render(camera);
    EXPECT_TRUE(function_run);
}

TEST_F(RenderableTests, test_renderable_instance_can_be_maintained_if_desired)
{
    glc::StaticScene scene;
    glc::Camera camera;
    static void const *this_in_scene = nullptr; // static global since on_render is const

    class RenderableTest : public glc::RenderableInterface
    {
    public:
        void on_render(const glc::Camera &,
                       const glc::UpdatableInfo &,
                       const glc::StaticScene &,
                       const glm::mat4 &) const final
        {
            this_in_scene = this;
        }
    };

    auto renderable = std::make_shared<RenderableTest>();
    scene.add_item(glc::Renderable(renderable));
    scene.render(camera);
    EXPECT_EQ(this_in_scene, renderable.get());
}

TEST_F(RenderableTests, renderable_still_called_when_positions_specified)
{
    glc::StaticScene scene;
    glc::Camera camera;
    static bool function_run = false; // static global since on_render is const

    class RenderableTest : public glc::RenderableInterface
    {
    public:
        void on_render(const glc::Camera &,
                       const glc::UpdatableInfo &,
                       const glc::StaticScene &,
                       const glm::mat4 &) const final
        {
            function_run = true;
        }
    };

    {
        std::stringstream err_stream;
        glc::testing::StreamRedirectGuard guard(err_stream.rdbuf(), std::cerr);

        scene.add_item(glc::Renderable(RenderableTest()), glc::Positions());

        auto err_msg = err_stream.str();
        EXPECT_NE(err_msg.find("WARNING: Renderable class specified;"), std::string::npos);
    }
    scene.render(camera);
    EXPECT_TRUE(function_run);
}
