// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/scene/StaticScene.hpp>
#include "GLInstance.hpp"
#include "TestUtils.hpp"
#include <gmock/gmock.h>

class StaticSceneTests : public ::testing::Test
{
    glc::testing::GLInstance gl_instance_;
};

TEST_F(StaticSceneTests, error_thrown_when_same_arguments_are_used_to_add_item_to_scene)
{
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Positions(), glc::Positions()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Normals(), glc::Normals()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::TexCoords(), glc::TexCoords()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::VertexColors(), glc::VertexColors()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Points(), glc::Points()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Lines(), glc::Lines()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::LineStrip(), glc::LineStrip()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Triangles(), glc::Triangles()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::TriangleStrip(), glc::TriangleStrip()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::TriangleFan(), glc::TriangleFan()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::DisplayType(), glc::DisplayType()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::ReadableID(), glc::ReadableID()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Visible(), glc::Visible()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Parent(), glc::Parent()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Transform(), glc::Transform()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::GlobalColor(), glc::GlobalColor()), std::runtime_error);
    }
}

TEST_F(StaticSceneTests, error_thrown_if_no_position_vector_is_present)
{
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Normals()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::TexCoords()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::VertexColors()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Points()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Lines()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::LineStrip()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Triangles()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::TriangleStrip()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::TriangleFan()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::DisplayType()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::ReadableID()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Visible()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Parent()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::Transform()), std::runtime_error);
    }
    {
        glc::StaticScene scene;
        ASSERT_THROW(scene.add_item(glc::GlobalColor()), std::runtime_error);
    }
}

TEST_F(StaticSceneTests, scene_items_are_deleted_properly)
{
    {
        glc::StaticScene scene;

        std::stringstream err_stream;
        glc::testing::StreamRedirectGuard stream_guard(err_stream.rdbuf(), std::cerr);

        EXPECT_FALSE(scene.remove_item(nullptr));

        auto err_msg = err_stream.str();
        EXPECT_NE(err_msg.find("WARNING:"), std::string::npos);
    }

    {
        glc::StaticScene scene;
        auto parent = scene.add_item(glc::Positions());
        auto child1 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child2 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child3 = scene.add_item(glc::Positions(), glc::Parent(child1));

        EXPECT_TRUE(scene.remove_item(parent, true));

        EXPECT_FALSE(scene.has_item(parent));
        EXPECT_FALSE(scene.has_item(child1));
        EXPECT_FALSE(scene.has_item(child2));
        EXPECT_FALSE(scene.has_item(child3));
    }

    {
        glc::StaticScene scene;
        auto parent = scene.add_item(glc::Positions());
        auto child1 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child2 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child3 = scene.add_item(glc::Positions(), glc::Parent(child1));

        EXPECT_TRUE(scene.remove_item(parent, false));

        EXPECT_FALSE(scene.has_item(parent));
        EXPECT_TRUE(scene.has_item(child1));
        EXPECT_TRUE(scene.has_item(child2));
        EXPECT_TRUE(scene.has_item(child3));
    }

    {
        glc::StaticScene scene;
        auto parent = scene.add_item(glc::Positions());
        auto child1 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child2 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child3 = scene.add_item(glc::Positions(), glc::Parent(child1));

        EXPECT_TRUE(scene.remove_item(parent, false));
        EXPECT_TRUE(scene.remove_item(child1, true));

        EXPECT_FALSE(scene.has_item(parent));
        EXPECT_FALSE(scene.has_item(child1));
        EXPECT_TRUE(scene.has_item(child2));
        EXPECT_FALSE(scene.has_item(child3));
    }

    {
        glc::StaticScene scene;
        auto parent = scene.add_item(glc::Positions());
        auto child1 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child2 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child3 = scene.add_item(glc::Positions(), glc::Parent(child1));

        EXPECT_TRUE(scene.remove_item(parent, false));
        EXPECT_TRUE(scene.remove_item(child1, false));

        EXPECT_FALSE(scene.has_item(parent));
        EXPECT_FALSE(scene.has_item(child1));
        EXPECT_TRUE(scene.has_item(child2));
        EXPECT_TRUE(scene.has_item(child3));
    }

    {
        glc::StaticScene scene;
        auto parent = scene.add_item(glc::Positions());
        auto child1 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child2 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child3 = scene.add_item(glc::Positions(), glc::Parent(child1));

        EXPECT_TRUE(scene.remove_item(parent, false));
        EXPECT_TRUE(scene.remove_item(child2));

        EXPECT_FALSE(scene.has_item(parent));
        EXPECT_TRUE(scene.has_item(child1));
        EXPECT_FALSE(scene.has_item(child2));
        EXPECT_TRUE(scene.has_item(child3));
    }

    {
        glc::StaticScene scene;
        auto parent = scene.add_item(glc::Positions());
        auto child1 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child2 = scene.add_item(glc::Positions(), glc::Parent(parent));
        auto child3 = scene.add_item(glc::Positions(), glc::Parent(child1));

        EXPECT_TRUE(scene.remove_item(parent, false));
        EXPECT_TRUE(scene.remove_item(child1, false));
        EXPECT_TRUE(scene.remove_item(child2, false));
        EXPECT_TRUE(scene.remove_item(child3, false));

        EXPECT_FALSE(scene.has_item(parent));
        EXPECT_FALSE(scene.has_item(child1));
        EXPECT_FALSE(scene.has_item(child2));
        EXPECT_FALSE(scene.has_item(child3));
    }
}

TEST_F(StaticSceneTests, update_and_get_properties)
{
    {
        glc::StaticScene scene;
        auto uuid = scene.add_item(glc::Positions());

        glc::DisplayMode expected = glc::DisplayMode::WHITE;
        scene.update_item(uuid, glc::DisplayType(expected));

        glc::DisplayMode actual;
        scene.get_item_info(uuid, glc::DisplayType(&actual));
        EXPECT_EQ(actual, expected);
    }

    {
        glc::StaticScene scene;
        auto uuid = scene.add_item(glc::Positions());

        glm::mat4 expected(glm::vec4(1, 2, 3, 4), glm::vec4(2, 4, 7, 3), glm::vec4(3, 7, 4, 3), glm::vec4(4, 3, 2, 1));
        scene.update_item(uuid, glc::Transform(expected));

        glm::mat4 actual;
        scene.get_item_info(uuid, glc::Transform(&actual));
        EXPECT_EQ(actual, expected);
    }

    {
        glc::StaticScene scene;
        auto uuid = scene.add_item(glc::Positions());

        std::string expected = "abcdefghijklmnopqrstuvwxyz01234567890";
        scene.update_item(uuid, glc::ReadableID(expected));

        std::string actual;
        scene.get_item_info(uuid, glc::ReadableID(&actual));
        EXPECT_EQ(actual, expected);
    }

    {
        glc::StaticScene scene;
        auto uuid = scene.add_item(glc::Positions());

        bool expected = false;
        scene.update_item(uuid, glc::Visible(expected));

        bool actual;
        scene.get_item_info(uuid, glc::Visible(&actual));
        EXPECT_EQ(actual, expected);
    }

    {
        glc::StaticScene scene;
        auto uuid = scene.add_item(glc::Positions());
        auto child = scene.add_item(glc::Positions(), glc::Parent(uuid));

        scene.update_item(uuid, glc::ChildrenVisible(false));

        bool actual;
        // parent is still visible
        scene.get_item_info(uuid, glc::Visible(&actual));
        EXPECT_TRUE(actual);

        // child is not visible
        scene.get_item_info(child, glc::Visible(&actual));
        EXPECT_FALSE(actual);
    }

    {
        glc::StaticScene scene;
        auto uuid = scene.add_item(glc::Positions());

        glm::vec3 expected = {1.f, 0.5f, 0.1f};
        scene.update_item(uuid, glc::GlobalColor(expected));

        glm::vec3 actual;
        scene.get_item_info(uuid, glc::GlobalColor(&actual));
        EXPECT_EQ(actual, expected);
    }
}

TEST_F(StaticSceneTests, remove_item_fails_if_item_does_not_exist)
{
    {
        glc::StaticScene scene;
        auto uuid = scene.add_item(glc::Positions());

        std::stringstream err_stream;
        glc::testing::StreamRedirectGuard stream_guard(err_stream.rdbuf(), std::cerr);

        EXPECT_TRUE(scene.remove_item(uuid, false));
        EXPECT_FALSE(scene.remove_item(uuid));

        auto err_msg = err_stream.str();
        EXPECT_NE(err_msg.find("WARNING:"), std::string::npos);
    }

    {
        glc::StaticScene scene;
        auto uuid = scene.add_item(glc::Positions());
        EXPECT_TRUE(scene.remove_item(uuid, true));
    }
}
