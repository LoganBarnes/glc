// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include "loop/SimpleLoop.hpp"
#include <glc/scene/StaticScene.hpp>
#include <glc/scene/SceneUtil.hpp>
#include <glc/PrimitiveMeshes.hpp>
#include <glc/GLCUtil.hpp>
#include <glm/gtc/matrix_transform.hpp>

class SceneExample : public examples::SimpleLoop
{
public:
    SceneExample()
        : examples::SimpleLoop("Wire Cube Example", 640, 480, true), camera_mover_(glc::Camera()), scene_trans_(1.f)
    {
        glc::set_common_defaults();

        constexpr int subdivisions = 100;

        std::vector<unsigned> indices;
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> tex_coords;

        {
            glc::build_cube_mesh(&indices, &vertices, &normals, &tex_coords);

            glm::mat4 trans = glm::translate(glm::mat4(1.f), glm::vec3(2.f, 0.f, 0.f))
                * glm::rotate(glm::mat4(1.f), glm::half_pi<float>() / 3.f, glm::vec3(0, 0, 1));
            cube_ = scene_.add_item(glc::Triangles(indices),
                                    glc::Positions(vertices),
                                    glc::Normals(normals),
                                    glc::TexCoords(tex_coords),
                                    glc::Transform(trans),
                                    glc::DisplayType(glc::DisplayMode::ADVANCED_SHADING),
                                    glc::ReadableID("Cube"),
                                    glc::GlobalColor(glm::vec3(0.3f, 0.3f, 1.f)));
        }

        glc::uid item2{};
        {
            glc::build_sphere_mesh(subdivisions, &indices, &vertices, nullptr, &tex_coords);

            glm::mat4 trans(1.f);
            trans[3] = glm::vec4(1.f);
            item2 = scene_.add_item(glc::Normals(vertices),
                                    glc::Positions(vertices),
                                    glc::Triangles(indices),
                                    glc::TexCoords(tex_coords),
                                    glc::DisplayType(glc::DisplayMode::ADVANCED_SHADING),
                                    glc::Parent(cube_),
                                    glc::Transform(trans),
                                    glc::ReadableID("Sphere"),
                                    glc::GlobalColor(glm::vec3(0.3f, 1.f, 0.3f)));
        }

        {
            glc::build_cylinder_mesh(subdivisions, &indices, &vertices, &normals, &tex_coords);

            glm::mat4 trans(1.f);
            trans[3] = glm::vec4(-1.5f, 0.f, -2.5f, 1.f);
            scene_.add_item(glc::Normals(normals),
                            glc::Triangles(indices),
                            glc::Positions(vertices),
                            glc::DisplayType(glc::DisplayMode::ADVANCED_SHADING),
                            glc::Parent(item2),
                            glc::TexCoords(tex_coords),
                            glc::Transform(trans),
                            glc::ReadableID("Cylinder"),
                            glc::GlobalColor(glm::vec3(1.f, 0.3f, 0.3f)));
        }

        {
            glc::build_cone_mesh(subdivisions * 2, &indices, &vertices, &normals, &tex_coords);

            glm::mat4 trans(1.f);
            trans[3] = glm::vec4(-1.f, 0.f, -0.5f, 1.f);
            cone_ = scene_.add_item(glc::Normals(normals),
                                    glc::Positions(vertices),
                                    glc::Triangles(indices),
                                    glc::DisplayType(glc::DisplayMode::ADVANCED_SHADING),
                                    glc::TexCoords(tex_coords),
                                    glc::Transform(trans),
                                    glc::Visible(true),
                                    glc::ReadableID("Cone"));
        }

        {
            glc::build_torus_mesh(subdivisions, 1.f, 0.25f, &indices, &vertices, &normals, &tex_coords);

            glm::mat4 trans(1.f);
            trans[3] = glm::vec4(0.f, 0.f, 1.f, 1.f);
            scene_.add_item(glc::Normals(normals),
                            glc::Positions(vertices),
                            glc::Triangles(indices),
                            glc::DisplayType(glc::DisplayMode::ADVANCED_SHADING),
                            glc::GlobalColor({1.f, 0.5f, 0.1f}),
                            glc::TexCoords(tex_coords),
                            glc::Transform(trans),
                            glc::Parent(cone_),
                            glc::ReadableID("Torus"));
        }

        glc::SceneUtil::add_axes_at_origin(scene_, 0.05f);

        scene_.add_light({500.f, 500.f, 500.f}, 1500000.f);
        scene_.add_light({-500.f, -500.f, 250.f}, 500000.f);
        scene_.add_light({250.f, -500.f, -500.f}, 500000.f);
        scene_.add_light({0, 500.f, -500.f}, 600000.f);

        // offset the camera so when we drag the mouse we orbit around the origin
        camera_mover_.set_zup(true);
        camera_mover_.set_origin_offset(5);
        camera_mover_.camera.set_aspect_ratio(get_framebuffer_width() * 1.f / get_framebuffer_height());

        glClearColor(0.1f, 0.1f, 0.1f, 1.f);
    }

    /**
     * Renders stuff
     */
    void render(int view_width, int view_height, float) const final
    {
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
        glViewport(0, 0, view_width, view_height);
        scene_.render(camera_mover_.camera);
    }

    void handle_mouse_drag(float deltaX, float deltaY) final
    {
        float scale_factor = 0.5f;
        camera_mover_.yaw(-deltaX * scale_factor);
        camera_mover_.pitch(deltaY * scale_factor);
    }

    void handle_scroll(float delta) final
    {
        float scale_factor = -2.5f;
        camera_mover_.zoom(delta * scale_factor);
    }

    void resize(int view_width, int view_height) final
    {
        camera_mover_.camera.set_aspect_ratio(view_width * 1.f / view_height);
    }

private:
    glc::CameraMover camera_mover_;
    glc::StaticScene scene_;
    glc::uid cube_;
    glc::uid cone_;
    glm::mat4 scene_trans_;
};

int main()
{
    SceneExample example;
    example.run_loop();
    return 0;
}
