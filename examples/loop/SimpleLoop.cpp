// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include "SimpleLoop.hpp"

#include <glc/GLC.hpp>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <iostream>
#include <chrono>
#include <algorithm>

namespace examples {

namespace {

#ifndef NDEBUG
#ifndef __APPLE__
void opengl_callback(GLenum /*source*/,
                     GLenum type,
                     GLuint /*id*/,
                     GLenum severity,
                     GLsizei /*length*/,
                     const GLchar *message,
                     const void * /*userParam*/)
{
    fprintf(stderr,
            "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
            type,
            severity,
            message);
}
#endif
#endif

} // namespace

SimpleLoop::SimpleLoop(const std::string &title, int width, int height, bool resizable, bool print_opengl_debug_info)
    : debug_opengl_(print_opengl_debug_info)
{
    // Set the error callback before any other GLFW calls so we get proper error reporting
    glfwSetErrorCallback([](int error, const char *description) {
        std::cerr << "ERROR: (" << error << ") " << description << std::endl;
    });

    init_glfw();
    create_window(title, width, height, resizable);

#ifndef NDEBUG
#ifndef __APPLE__
    if (debug_opengl_) {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(static_cast<GLDEBUGPROC>(opengl_callback), nullptr);
    }
#endif
#endif

    set_callbacks();
    resize(width, height);
}

SimpleLoop::~SimpleLoop() = default;

void SimpleLoop::run_loop()
{
    auto currentTime = std::chrono::steady_clock::now();
    double accumulator = 0.0;

    do {
        auto newTime = std::chrono::steady_clock::now();
        double frameTime = std::chrono::duration<double>{newTime - currentTime}.count();
        currentTime = newTime;

        frameTime = std::min(0.1, frameTime);

        if (!paused_) {
            accumulator += frameTime;

            while (accumulator >= time_step_) {
                update(static_cast<float>(sim_time_), static_cast<float>(time_step_));
                sim_time_ += time_step_;
                accumulator -= time_step_;
            }
        }

        const double alpha = accumulator / time_step_;

        int w, h;
        glfwGetFramebufferSize(window_.get(), &w, &h);
        render(w, h, static_cast<float>(alpha));

        glfwSwapBuffers(window_.get());

        if (paused_) {
            glfwWaitEvents();
        } else {
            glfwPollEvents();
        }
    } while (!glfwWindowShouldClose(window_.get()));
}

int SimpleLoop::get_framebuffer_width() const
{
    int w, h;
    glfwGetFramebufferSize(window_.get(), &w, &h);
    return w;
}

int SimpleLoop::get_framebuffer_height() const
{
    int w, h;
    glfwGetFramebufferSize(window_.get(), &w, &h);
    return h;
}

void SimpleLoop::init_glfw()
{
    up_glfw_ = std::unique_ptr<int, std::function<void(int *)>>(new int(glfwInit()), [](auto p) {
        glfwTerminate();
        delete p;
    });

    if (*up_glfw_ == 0) {
        throw std::runtime_error("GLFW init failed");
    }
}

void SimpleLoop::create_window(const std::string &title, int width, int height, bool resizable)
{
    glfwWindowHint(GLFW_RESIZABLE, resizable);

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
#ifdef __APPLE__
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
#else
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
#endif // __APPLE__

    window_ = std::unique_ptr<GLFWwindow, std::function<void(GLFWwindow *)>>( //
        glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr),
        [](auto p) {
            if (p) {
                glfwDestroyWindow(p);
            }
        });

    if (window_ == nullptr) {
        throw std::runtime_error("GLFW window creation failed");
    }

    glfwMakeContextCurrent(window_.get());
    glfwSwapInterval(1);

    if (gl3wInit()) {
        throw std::runtime_error("Failed to initialize OpenGL context");
    }
}

void SimpleLoop::set_callbacks()
{
    glfwSetWindowUserPointer(window_.get(), this);

    glfwSetFramebufferSizeCallback(window_.get(), [](GLFWwindow *window, int width, int height) {
        static_cast<SimpleLoop *>(glfwGetWindowUserPointer(window))->resize(width, height);
    });

    glfwSetMouseButtonCallback(window_.get(), [](GLFWwindow *window, int button, int action, int) {
        auto simple_loop = static_cast<SimpleLoop *>(glfwGetWindowUserPointer(window));
        if (button == GLFW_MOUSE_BUTTON_1) {
            if (action == GLFW_PRESS) {
                simple_loop->left_mouse_down_ = true;
                glfwGetCursorPos(window, &simple_loop->prev_mouseX_, &simple_loop->prev_mouseY_);
            } else if (action == GLFW_RELEASE) {
                simple_loop->left_mouse_down_ = false;
            }
        }
    });

    glfwSetKeyCallback(window_.get(), [](GLFWwindow *window, int key, int, int action, int) {
        auto simple_loop = static_cast<SimpleLoop *>(glfwGetWindowUserPointer(window));
        if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        }
        if (key == GLFW_KEY_P && action == GLFW_RELEASE) {
            simple_loop->paused_ = !simple_loop->paused_;
        }
    });

    glfwSetCursorPosCallback(window_.get(), [](GLFWwindow *window, double xpos, double ypos) {
        auto simple_loop = static_cast<SimpleLoop *>(glfwGetWindowUserPointer(window));
        if (simple_loop->left_mouse_down_) {
            simple_loop->handle_mouse_drag(static_cast<float>(xpos - simple_loop->prev_mouseX_),
                                           static_cast<float>(simple_loop->prev_mouseY_ - ypos));
            simple_loop->prev_mouseX_ = xpos;
            simple_loop->prev_mouseY_ = ypos;
        }
    });

    glfwSetScrollCallback(window_.get(), [](GLFWwindow *window, double, double yoffset) {
        static_cast<SimpleLoop *>(glfwGetWindowUserPointer(window))->handle_scroll(static_cast<float>(yoffset));
    });
}

} // namespace examples
