// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/camera/CameraMover.hpp>
#include <glc/Ray.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <limits>
#include <utility>

namespace glc {
namespace detail {

namespace {

// ray at origin 'o' with direction 'd'
// plane at point 'p' with normal 'n'
template <typename T>
T intersect_plane(const glm::vec<3, T> &o, const glm::vec<3, T> &d, glm::vec<3, T> p, const glm::vec<3, T> &n)
{
    // plane ray intersection
    const T denom = glm::dot(d, n);

    p -= o;
    const T t = glm::dot(p, n) / denom;

    if (t >= T(0) && t != std::numeric_limits<T>::infinity()) {
        return t;
    }
    return std::numeric_limits<T>::infinity();
}

} // namespace

template <typename T>
CameraMover<T>::CameraMover(Camera<T> cam, bool zup)
    : camera(std::move(cam))
    , origin_(0)
    , pitchDegrees_(0)
    , yawDegrees_(0)
    , offset_(5)
    , fruMoveDir_(0)
    , moveAmt_(1)
    , zup_(zup)
{}

template <typename T>
void CameraMover<T>::update()
{

    // set look direction
    auto look_vec = (zup_ ? glm::vec<3, T>(0, 1, 0) : glm::vec<3, T>(0, 0, -1));
    auto neg_up_vec = (zup_ ? glm::vec<3, T>(0, 0, -1) : glm::vec<3, T>(0, -1, 0));
    glm::vec<3, T> right_vec(1, 0, 0);
    look_vec = glm::rotate(look_vec, glm::radians(pitchDegrees_), -right_vec);
    look_vec = glm::rotate(look_vec, glm::radians(yawDegrees_), neg_up_vec);
    right_vec = glm::rotate(right_vec, glm::radians(yawDegrees_), neg_up_vec);

    glm::vec<3, T> up = glm::cross(right_vec, look_vec);

    // update origin
    if (fruMoveDir_.x != 0 || fruMoveDir_.y != 0 || fruMoveDir_.z != 0) {
        glm::vec<3, T> move_dist(fruMoveDir_.x, fruMoveDir_.y, fruMoveDir_.z);
        move_dist = glm::normalize(move_dist) * moveAmt_;
        origin_ += look_vec * move_dist.x;
        origin_ += right_vec * move_dist.y;
        origin_ += up * move_dist.z;
    }

    glm::vec<3, T> point = origin_ + look_vec;
    glm::vec<3, T> eye = origin_ - look_vec * offset_;

    camera.look_at(eye, point, up);
}

template <typename T>
void CameraMover<T>::drag_with_mouse_delta(float deltaX, float deltaY)
{
    yawDegrees_ += deltaX * 0.22;
    pitchDegrees_ += deltaY * 0.22;

    pitchDegrees_ = glm::clamp(pitchDegrees_, T(-90), T(90));
    update();
}

template <typename T>
void CameraMover<T>::scroll_with_delta(float /*deltaX*/, float deltaY)
{
    offset_ += deltaY;
    offset_ = glm::max(T(0), offset_);
    update();
}

template <typename T>
void CameraMover<T>::pan_from_clip_pos(const glm::vec<2, T> &start_pos, const glm::vec<2, T> &end_pos)
{
    glm::vec<3, T> n = -camera.get_look_vec();

    auto world_from_clip = [&](const auto &clip_pos, T *dist) {
        auto ray = camera.get_ray_from_clip_pos(clip_pos);
        *dist = intersect_plane(ray.origin, ray.direction, origin_, n);
        return ray.origin + ray.direction * (*dist);
    };

    T t;
    auto world_start = world_from_clip(start_pos, &t);
    if (t == std::numeric_limits<T>::infinity()) {
        return;
    }

    auto world_end = world_from_clip(end_pos, &t);
    if (t == std::numeric_limits<T>::infinity()) {
        return;
    }

    auto diff = world_end - world_start;

    origin_ -= diff;
    update();
}

template <typename T>
void CameraMover<T>::add_update_move_dir(glm::ivec3 forwardRightUp)
{
    fruMoveDir_ += forwardRightUp;
    glm::clamp(fruMoveDir_, glm::ivec3(-1), glm::ivec3(1));
    update();
}

template <typename T>
void CameraMover<T>::set_origin(glm::vec<3, T> origin)
{
    origin_ = origin;
    update();
}

template <typename T>
void CameraMover<T>::set_pitch_degrees(T pitch)
{
    pitchDegrees_ = pitch;
    update();
}

template <typename T>
void CameraMover<T>::set_yaw_degrees(T yaw)
{
    yawDegrees_ = yaw;
    update();
}

template <typename T>
void CameraMover<T>::pitch(T pitch_delta)
{
    set_pitch_degrees(get_pitch_degrees() - pitch_delta);
}

template <typename T>
void CameraMover<T>::yaw(T yaw_delta)
{
    set_yaw_degrees(get_yaw_degrees() - yaw_delta);
}

template <typename T>
void CameraMover<T>::zoom(T zoom_delta)
{
    set_origin_offset(get_origin_offset() + zoom_delta);
}

template <typename T>
void CameraMover<T>::set_origin_offset(T offset)
{
    offset_ = std::max(T(0), offset);
    update();
}

template <typename T>
void CameraMover<T>::set_update_move_distance(T distance)
{
    moveAmt_ = distance;
}

template <typename T>
void CameraMover<T>::set_zup(bool zup)
{
    zup_ = zup;
    update();
}

template <typename T>
void CameraMover<T>::set_look_along_x(bool positive)
{
    pitchDegrees_ = 0.f;
    yawDegrees_ = positive ? 90.f : -90.f;
    update();
}

template <typename T>
void CameraMover<T>::set_look_along_y(bool positive)
{
    pitchDegrees_ = 0.f;
    yawDegrees_ = positive ? 180.f : 0.f;
    update();
}

template <typename T>
void CameraMover<T>::set_look_along_z(bool positive)
{
    pitchDegrees_ = positive ? -90.f : 90.f;
    yawDegrees_ = 90.f;
    update();
}

template <typename T>
void CameraMover<T>::set_look_along_xyz()
{
    pitchDegrees_ = 45.f;
    yawDegrees_ = -45.f;
    update();
}

template <typename T>
void CameraMover<T>::rotate_45_deg_around_z(bool positive)
{
    yawDegrees_ += positive ? -45.f : 45.f;
    update();
}

template <typename T>
T CameraMover<T>::get_pitch_degrees() const
{
    return pitchDegrees_;
}

template <typename T>
T CameraMover<T>::get_yaw_degrees() const
{
    return yawDegrees_;
}

template <typename T>
T CameraMover<T>::get_origin_offset() const
{
    return offset_;
}

template <typename T>
glm::vec<3, T> CameraMover<T>::get_origin() const
{
    return origin_;
}

} // namespace detail

template class detail::CameraMover<float>;
template class detail::CameraMover<double>;

} // namespace glc
