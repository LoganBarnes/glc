// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/camera/Camera.hpp>
#include <glc/Ray.hpp>

namespace glc {
namespace detail {

template <typename T>
Camera<T>::Camera(bool use_perspective, bool link_projection_matrices)
    : view_data_{glc::look_at<T>({0, 0, 5}, {0, 0, 0}, {0, 1, 0})}
    , pers_cam_{glc::perspective<T>(60, 1, T(0.1), 500)}
    , orth_cam_{glc::ortho<T>(-1, 1, -1, 1, T(0.1), 500)}
    , use_perspective_{use_perspective}
    , link_projection_matrices_{link_projection_matrices}
{
    update_matrices();
}

template <typename T>
void Camera<T>::handle_screen_change(int width, int height)
{
    set_aspect_ratio(T(width) / T(height));
    check_link_and_update();
}

template <typename T>
void Camera<T>::look_at(glm::vec<3, T> eye, glm::vec<3, T> point, glm::vec<3, T> up)
{
    view_data_ = glc::look_at<T>(eye, point, up);
    check_link_and_update();
}

template <typename T>
void Camera<T>::perspective(T fovy_degrees, T aspect, T near, T far)
{
    pers_cam_ = glc::perspective(fovy_degrees, aspect, near, far);
    update_matrices();
}

template <typename T>
void Camera<T>::ortho(T left, T right, T bottom, T top)
{
    ortho(left, right, bottom, top, orth_cam_.ortho_near, orth_cam_.ortho_far);
}

template <typename T>
void Camera<T>::ortho(T left, T right, T bottom, T top, T near, T far)
{
    orth_cam_ = glc::ortho(left, right, bottom, top, near, far);
    update_matrices();
}

/**
 * SETTERS
 */

template <typename T>
void Camera<T>::set_eye_pos(const glm::vec<3, T> &eye)
{
    look_at(eye, eye + view_data_.look_vec, view_data_.up_vec);
}
template <typename T>
void Camera<T>::set_look_vec(const glm::vec<3, T> &look)
{
    look_at(view_data_.eye_pos, view_data_.eye_pos + look, view_data_.up_vec);
}
template <typename T>
void Camera<T>::set_up_vec(const glm::vec<3, T> &up)
{
    look_at(view_data_.eye_pos, view_data_.eye_pos + view_data_.look_vec, up);
}
template <typename T>
void Camera<T>::set_fovy_degrees(T fovy_degrees)
{
    perspective(fovy_degrees, pers_cam_.aspect_ratio, pers_cam_.near_plane, pers_cam_.far_plane);
}
template <typename T>
void Camera<T>::set_aspect_ratio(T aspect_ratio)
{
    perspective(pers_cam_.fovy_degrees, aspect_ratio, pers_cam_.near_plane, pers_cam_.far_plane);
}
template <typename T>
void Camera<T>::set_near_plane_distance(T near)
{
    perspective(pers_cam_.fovy_degrees, pers_cam_.aspect_ratio, near, pers_cam_.far_plane);
}
template <typename T>
void Camera<T>::set_far_plane_distance(T far)
{
    perspective(pers_cam_.fovy_degrees, pers_cam_.aspect_ratio, pers_cam_.near_plane, far);
}
template <typename T>
void Camera<T>::set_ortho_left(T left)
{
    ortho(left, orth_cam_.ortho_right, orth_cam_.ortho_bottom, orth_cam_.ortho_top);
}
template <typename T>
void Camera<T>::set_ortho_right(T right)
{
    ortho(orth_cam_.ortho_left, right, orth_cam_.ortho_bottom, orth_cam_.ortho_top);
}
template <typename T>
void Camera<T>::set_ortho_bottom(T bottom)
{
    ortho(orth_cam_.ortho_left, orth_cam_.ortho_right, bottom, orth_cam_.ortho_top);
}
template <typename T>
void Camera<T>::set_ortho_top(T top)
{
    ortho(orth_cam_.ortho_left, orth_cam_.ortho_right, orth_cam_.ortho_bottom, top);
}
template <typename T>
void Camera<T>::set_ortho_near(T near)
{
    ortho(orth_cam_.ortho_left,
          orth_cam_.ortho_right,
          orth_cam_.ortho_bottom,
          orth_cam_.ortho_top,
          near,
          orth_cam_.ortho_far);
}
template <typename T>
void Camera<T>::set_ortho_far(T far)
{
    ortho(orth_cam_.ortho_left,
          orth_cam_.ortho_right,
          orth_cam_.ortho_bottom,
          orth_cam_.ortho_top,
          orth_cam_.ortho_near,
          far);
}
template <typename T>
void Camera<T>::set_use_perspective(bool use_perspective)
{
    use_perspective_ = use_perspective;
}
template <typename T>
void Camera<T>::set_link_projection_matrices(bool link_projection_matrices)
{
    link_projection_matrices_ = link_projection_matrices;
}

/**
 * GETTERS
 */

template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_clip_from_view_matrix() const
{
    return use_perspective_ ? get_perspective_clip_from_view_matrix() : get_orthographic_clip_from_view_matrix();
}

template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_clip_from_world_matrix() const
{
    return use_perspective_ ? get_perspective_clip_from_world_matrix() : get_orthographic_clip_from_world_matrix();
}

template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_world_from_clip_matrix() const
{
    return use_perspective_ ? get_perspective_world_from_clip_matrix() : get_orthographic_world_from_clip_matrix();
}
template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_perspective_clip_from_view_matrix() const
{
    return pers_cam_.projection_matrix;
}
template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_perspective_clip_from_world_matrix() const
{
    return perspective_projection_view_matrix_;
}
template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_perspective_world_from_clip_matrix() const
{
    return perspective_inverse_scale_view_matrix_;
}
template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_orthographic_clip_from_view_matrix() const
{
    return orth_cam_.projection_matrix;
}
template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_orthographic_clip_from_world_matrix() const
{
    return orthographic_projection_view_matrix_;
}
template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_orthographic_world_from_clip_matrix() const
{
    return orthographic_inverse_scale_view_matrix_;
}
template <typename T>
const glm::mat<4, 4, T> &Camera<T>::get_view_from_world_matrix() const
{
    return view_data_.view_matrix;
}
template <typename T>
const glm::vec<3, T> &Camera<T>::get_eye_pos() const
{
    return view_data_.eye_pos;
}
template <typename T>
const glm::vec<3, T> &Camera<T>::get_look_vec() const
{
    return view_data_.look_vec;
}
template <typename T>
const glm::vec<3, T> &Camera<T>::get_up_vec() const
{
    return view_data_.up_vec;
}
template <typename T>
const glm::vec<3, T> &Camera<T>::get_right_vec() const
{
    return view_data_.right_vec;
}
template <typename T>
const T &Camera<T>::get_focal_dist() const
{
    return view_data_.focal_dist;
}
template <typename T>
const T &Camera<T>::get_fovy_degrees() const
{
    return pers_cam_.fovy_degrees;
}
template <typename T>
const T &Camera<T>::get_fovy_radians() const
{
    return pers_cam_.fovy_radians;
}
template <typename T>
const T &Camera<T>::get_aspect_ratio() const
{
    return pers_cam_.aspect_ratio;
}
template <typename T>
const T &Camera<T>::get_near_plane_distance() const
{
    return pers_cam_.near_plane;
}
template <typename T>
const T &Camera<T>::get_far_plane_distance() const
{
    return pers_cam_.far_plane;
}
template <typename T>
const T &Camera<T>::get_ortho_left() const
{
    return orth_cam_.ortho_left;
}
template <typename T>
const T &Camera<T>::get_ortho_right() const
{
    return orth_cam_.ortho_right;
}
template <typename T>
const T &Camera<T>::get_ortho_bottom() const
{
    return orth_cam_.ortho_bottom;
}
template <typename T>
const T &Camera<T>::get_ortho_top() const
{
    return orth_cam_.ortho_top;
}
template <typename T>
const T &Camera<T>::get_ortho_near() const
{
    return orth_cam_.ortho_near;
}
template <typename T>
const T &Camera<T>::get_ortho_far() const
{
    return orth_cam_.ortho_far;
}
template <typename T>
bool Camera<T>::is_using_perspective() const
{
    return use_perspective_;
}
template <typename T>
bool Camera<T>::is_linking_projection_matrices() const
{
    return link_projection_matrices_;
}

template <typename T>
glc::ray<3, T> Camera<T>::get_ray_from_clip_pos(const glm::vec<2, T> &clip_pos) const
{

    if (is_using_perspective()) {
        glm::vec<3, T> far_point = glm::vec<3, T>(get_world_from_clip_matrix() * glm::vec<4, T>(clip_pos, T(-1), T(1)));
        return glc::ray<3, T>(get_eye_pos(), glm::normalize(far_point - get_eye_pos()));
    }

    glm::vec<3, T> near_point = glm::vec<3, T>(get_world_from_clip_matrix() * glm::vec<4, T>(clip_pos, T(0), T(1)));
    return glc::ray<3, T>(near_point + get_look_vec() * get_ortho_near(), get_look_vec());
}

template <typename T>
glc::ray<3, T> Camera<T>::get_ray_from_clip_pos(T x, T y) const
{
    return get_ray_from_clip_pos(glm::vec<2, T>{x, y});
}

template <typename T>
void Camera<T>::update_matrices()
{
    perspective_projection_view_matrix_ = pers_cam_.projection_matrix * view_data_.view_matrix;
    perspective_inverse_scale_view_matrix_ = glm::inverse(pers_cam_.scale_matrix * view_data_.view_matrix);
    orthographic_projection_view_matrix_ = orth_cam_.projection_matrix * view_data_.view_matrix;
    orthographic_inverse_scale_view_matrix_ = glm::inverse(orth_cam_.scale_matrix * view_data_.view_matrix);
}

template <typename T>
void Camera<T>::check_link_and_update()
{
    // recalculate orthographic projection size based on fov and focal distance
    if (link_projection_matrices_) {
        T h = glm::tan(pers_cam_.fovy_radians * T(0.5)) * view_data_.focal_dist;
        T w = pers_cam_.aspect_ratio * h;

        ortho(-w, w, -h, h);
    } else {
        update_matrices();
    }
}

} // namespace detail

template class detail::Camera<float>;
template class detail::Camera<double>;

} // namespace glc
