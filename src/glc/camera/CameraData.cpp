// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/camera/CameraData.hpp>
#include <glm/gtc/matrix_transform.hpp> // glm::lookAt()

namespace glc {

template <typename T>
ViewData<T> look_at(glm::vec<3, T> eye, glm::vec<3, T> point, glm::vec<3, T> up)
{
    glm::vec<3, T> look = point - eye;
    T dist = glm::length(look);

    look /= dist; // normalize;
    glm::vec<3, T> right = glm::cross(look, up);
    glm::mat<4, 4, T> view_matrix = glm::lookAt(eye, point, up);

    return ViewData<T>{eye, look, up, right, view_matrix, dist};
}

template <typename T>
PerspectiveData<T> perspective(T fovy_degrees, T aspect, T near, T far)
{
    T fovy_radians = glm::radians(fovy_degrees);

    glm::mat<4, 4, T> projection_matrix = glm::perspective(fovy_radians, aspect, near, far);

    T h = far * glm::tan(fovy_radians * 0.5f);
    T w = aspect * h;

    // row-major order
    glm::mat<4, 4, T> scale_matrix{T(1) / w, 0, 0, 0, 0, T(1) / h, 0, 0, 0, 0, T(1) / far, 0, 0, 0, 0, 1};

    return PerspectiveData<T>{fovy_degrees, fovy_radians, aspect, near, far, projection_matrix, scale_matrix};
}

template <typename T>
OrthographicData<T> ortho(T left, T right, T bottom, T top, T near, T far)
{

    glm::mat<4, 4, T> projection_matrix = glm::ortho(left, right, bottom, top, near, far);

    T h = T(0.5) * (top - bottom);
    T w = T(0.5) * (right - left);

    glm::mat<4, 4, T> scale_matrix
        = glm::mat<4, 4, T>(T(1) / w, 0, 0, 0, 0, T(1) / h, 0, 0, 0, 0, T(1) / far, 0, 0, 0, 0, 1);

    return OrthographicData<T>{left, right, bottom, top, near, far, projection_matrix, scale_matrix};
}

template ViewData<float> look_at(glm::vec3, glm::vec3, glm::vec3);
template ViewData<double> look_at(glm::dvec3, glm::dvec3, glm::dvec3);

template PerspectiveData<float> perspective(float, float, float, float);
template PerspectiveData<double> perspective(double, double, double, double);

template OrthographicData<float> ortho(float, float, float, float, float, float);
template OrthographicData<double> ortho(double, double, double, double, double, double);

} // namespace glc
