// ///////////////////////////////////////////////////////////////////////////////////////
//                                                                           |________|
//  Copyright (c) 2018 CloudNC Ltd - All Rights Reserved                        |  |
//                                                                              |__|
//        ____                                                                .  ||
//       / __ \                                                               .`~||$$$$
//      | /  \ \         /$$$$$$  /$$                           /$$ /$$   /$$  /$$$$$$$
//      \ \ \ \ \       /$$__  $$| $$                          | $$| $$$ | $$ /$$__  $$
//    / / /  \ \ \     | $$  \__/| $$  /$$$$$$  /$$   /$$  /$$$$$$$| $$$$| $$| $$  \__/
//   / / /    \ \__    | $$      | $$ /$$__  $$| $$  | $$ /$$__  $$| $$ $$ $$| $$
//  / / /      \__ \   | $$      | $$| $$  \ $$| $$  | $$| $$  | $$| $$  $$$$| $$
// | | / ________ \ \  | $$    $$| $$| $$  | $$| $$  | $$| $$  | $$| $$\  $$$| $$    $$
//  \ \_/ ________/ /  |  $$$$$$/| $$|  $$$$$$/|  $$$$$$/|  $$$$$$$| $$ \  $$|  $$$$$$/
//   \___/ ________/    \______/ |__/ \______/  \______/  \_______/|__/  \__/ \______/
//
// ///////////////////////////////////////////////////////////////////////////////////////
#include "glc/PrimitiveMeshes.hpp"
#include <glm/gtc/constants.hpp>
#include <algorithm>

namespace glc {

void build_cube_mesh(std::vector<unsigned> *triangles,
                     std::vector<glm::vec3> *vertices,
                     std::vector<glm::vec3> *normals,
                     std::vector<glm::vec2> *tex_coords)
{
    assert(triangles);
    assert(vertices);

    std::vector<unsigned> tris;
    std::vector<glm::vec3> verts;
    std::vector<glm::vec3> norms;
    std::vector<glm::vec2> tcoords;

    for (int i = 0; i < 6; ++i) {
        tcoords.insert(tcoords.end(),
                       {glm::vec2{0.f, 0.f}, glm::vec2{1.f, 0.f}, glm::vec2{0.f, 1.f}, glm::vec2{1.f, 1.f}});
    }

    auto insert_norm_and_tris = [&](const glm::vec3 &n, unsigned offset) {
        norms.insert(norms.end(), 4, n);
        tris.insert(tris.end(), {offset + 1, offset + 0, offset + 2, offset + 2, offset + 3, offset + 1});
    };

    float radius = 0.5f;

    // Pos-X face
    verts.emplace_back(radius, -radius, radius);
    verts.emplace_back(radius, radius, radius);
    verts.emplace_back(radius, -radius, -radius);
    verts.emplace_back(radius, radius, -radius);
    insert_norm_and_tris(glm::vec3(1.f, 0.f, 0.f), 0);

    // Pos-Y face
    verts.emplace_back(radius, radius, radius);
    verts.emplace_back(-radius, radius, radius);
    verts.emplace_back(radius, radius, -radius);
    verts.emplace_back(-radius, radius, -radius);
    insert_norm_and_tris(glm::vec3(0.f, 1.f, 0.f), 4);

    // Neg-X face
    verts.emplace_back(-radius, radius, radius);
    verts.emplace_back(-radius, -radius, radius);
    verts.emplace_back(-radius, radius, -radius);
    verts.emplace_back(-radius, -radius, -radius);
    insert_norm_and_tris(glm::vec3(-1.f, 0.f, 0.f), 8);

    // Neg-Y face
    verts.emplace_back(-radius, -radius, radius);
    verts.emplace_back(radius, -radius, radius);
    verts.emplace_back(-radius, -radius, -radius);
    verts.emplace_back(radius, -radius, -radius);
    insert_norm_and_tris(glm::vec3(0.f, -1.f, 0.f), 12);

    // Pos-Z face
    verts.emplace_back(-radius, radius, radius);
    verts.emplace_back(radius, radius, radius);
    verts.emplace_back(-radius, -radius, radius);
    verts.emplace_back(radius, -radius, radius);
    insert_norm_and_tris(glm::vec3(0.f, 0.f, 1.f), 16);

    // Neg-Z face
    verts.emplace_back(-radius, -radius, -radius);
    verts.emplace_back(radius, -radius, -radius);
    verts.emplace_back(-radius, radius, -radius);
    verts.emplace_back(radius, radius, -radius);
    insert_norm_and_tris(glm::vec3(0.f, 0.f, -1.f), 20);

    // hasty check that indices use all vertices and don't overshoot
    assert(*std::max_element(tris.begin(), tris.end()) == verts.size() - 1);

    *triangles = std::move(tris);
    *vertices = std::move(verts);

    if (normals) {
        *normals = std::move(norms);
    }
    if (tex_coords) {
        *tex_coords = std::move(tcoords);
    }
}

void build_cone_mesh(unsigned divisions,
                     std::vector<unsigned> *triangles,
                     std::vector<glm::vec3> *vertices,
                     std::vector<glm::vec3> *normals,
                     std::vector<glm::vec2> *tex_coords)
{
    assert(triangles);
    assert(vertices);
    divisions = std::max(3u, divisions);

    std::vector<unsigned> tris;
    std::vector<glm::vec3> verts;
    std::vector<glm::vec3> norms;
    std::vector<glm::vec2> tcoords;

    // Bottom face
    {
        glm::vec3 n(0.f, 0.f, -1.f);

        verts.emplace_back(0.f, 0.f, -0.5f);
        norms.emplace_back(n);
        tcoords.emplace_back(0.5f, 0.5f);

        for (unsigned i = 0; i <= divisions; ++i) {
            float angle = i * glm::two_pi<float>() / divisions;

            glm::vec2 p2(std::cos(angle), std::sin(angle));
            glm::vec2 uv = p2 * 0.5f + glm::vec2(0.5f);

            verts.emplace_back(p2 * 0.5f, -0.5f);
            norms.emplace_back(n);
            tcoords.emplace_back(uv);
        }
    }

    // Cone
    // Create a series of triangles around the cone's axis (Z-axis). The bottom two points
    // lie on the cone's base and the top point will be the cone's point.
    //
    // The normals for each of the bottom points are determined by their position relative to the
    // origin. The normal of the top point is just interpolated value of the bottom two points.
    //
    // The radius of the cone's base is 0.5 and the height of the cone is 1.0. This means the tangent
    // vector to the surface will have a height of 1 and a horizontal length of 0.5. Since the normals
    // are perpendicular to the tangent vector they will have a height 0.5 and a vertical length of 1.
    // This vector is then normalized to create a unit normal.
    auto prev_n = glm::normalize(glm::vec3(1.f, 0.f, 0.5f));
    auto prev_u = 0.f;

    verts.emplace_back(0.5f, 0.f, -0.5f);
    norms.emplace_back(prev_n);
    tcoords.emplace_back(0.f, 1.f);

    for (unsigned i = 1; i <= divisions; ++i) {
        float next_u = i * 1.f / divisions;
        float theta = next_u * glm::two_pi<float>();

        auto xy = glm::vec2(std::cos(theta), std::sin(theta));
        auto p = glm::vec3(xy, -1.f) * 0.5f;
        auto next_n = glm::normalize(glm::vec3(xy, 0.5f));

        // top point
        verts.emplace_back(0.f, 0.f, 0.5f);
        norms.emplace_back(glm::normalize(glm::mix(prev_n, next_n, 0.5f)));
        tcoords.emplace_back(glm::mix(prev_u, next_u, 0.5f), 0.f);

        // bottom
        verts.emplace_back(p);
        norms.emplace_back(next_n);
        tcoords.emplace_back(next_u, 1.f);

        prev_n = next_n;
        prev_u = next_u;
    }

    // Bottom face
    for (unsigned i = 0; i < divisions; ++i) {
        tris.emplace_back(0);
        tris.emplace_back(i + 2);
        tris.emplace_back(i + 1);
    }

    // Cone
    unsigned offset = divisions + 2;
    for (unsigned i = 0; i < divisions; ++i) {
        tris.emplace_back(offset + i * 2 + 0);
        tris.emplace_back(offset + i * 2 + 2);
        tris.emplace_back(offset + i * 2 + 1);
    }

    // hasty check that indices use all vertices and don't overshoot
    assert(*std::max_element(tris.begin(), tris.end()) == verts.size() - 1);

    *triangles = std::move(tris);
    *vertices = std::move(verts);

    if (normals) {
        *normals = std::move(norms);
    }
    if (tex_coords) {
        *tex_coords = std::move(tcoords);
    }
}

void build_cylinder_mesh(unsigned divisions,
                         std::vector<unsigned> *triangles,
                         std::vector<glm::vec3> *vertices,
                         std::vector<glm::vec3> *normals,
                         std::vector<glm::vec2> *tex_coords)
{
    assert(triangles);
    assert(vertices);
    divisions = std::max(3u, divisions);

    std::vector<unsigned> tris;
    std::vector<glm::vec3> verts;
    std::vector<glm::vec3> norms;
    std::vector<glm::vec2> tcoords;

    // Top face
    glm::vec3 n(0.f, 0.f, 1.f);

    verts.emplace_back(0.f, 0.f, 0.5f);
    norms.emplace_back(n);
    tcoords.emplace_back(0.5f, 0.5f);

    for (unsigned i = 0; i <= divisions; ++i) {
        float angle = i * glm::two_pi<float>() / divisions;

        glm::vec2 p2(std::cos(angle), std::sin(angle));
        glm::vec2 uv = p2 * 0.5f + glm::vec2(0.5f);

        verts.emplace_back(p2 * 0.5f, 0.5f);
        norms.emplace_back(n);
        tcoords.emplace_back(uv.x, 1.f - uv.y);
    }

    // Bottom face
    n.z = -1.f;

    verts.emplace_back(0.f, 0.f, -0.5f);
    norms.emplace_back(n);
    tcoords.emplace_back(0.5f, 0.5f);

    for (unsigned i = 0; i <= divisions; ++i) {
        float angle = i * glm::two_pi<float>() / divisions;

        glm::vec2 p2(std::cos(angle), std::sin(angle));
        glm::vec2 uv = p2 * 0.5f + glm::vec2(0.5f);

        verts.emplace_back(p2 * 0.5f, -0.5f);
        norms.emplace_back(n);
        tcoords.emplace_back(uv);
    }

    // Outer wall
    for (unsigned i = 0; i <= divisions; ++i) {
        float u = i * 1.f / divisions;
        float theta = u * glm::two_pi<float>();

        n = glm::vec3(std::cos(theta), std::sin(theta), 0.f);
        glm::vec3 p = n * 0.5f;

        // top point
        verts.emplace_back(p.x, p.y, 0.5f);
        norms.emplace_back(n);
        tcoords.emplace_back(u, 0.f);

        // bottom point
        verts.emplace_back(p.x, p.y, -0.5f);
        norms.emplace_back(n);
        tcoords.emplace_back(u, 1.f);
    }

    // Top face
    for (unsigned i = 0; i < divisions; ++i) {
        tris.emplace_back(0);
        tris.emplace_back(i + 1);
        tris.emplace_back(i + 2);
    }

    // Bottom face
    unsigned offset = divisions + 2;
    for (unsigned i = 0; i < divisions; ++i) {
        tris.emplace_back(offset);
        tris.emplace_back(offset + i + 2);
        tris.emplace_back(offset + i + 1);
    }

    // Outer wall
    offset = (divisions + 2) * 2;
    for (unsigned i = 0; i < divisions; ++i) {
        tris.emplace_back(offset + i * 2 + 0);
        tris.emplace_back(offset + i * 2 + 1);
        tris.emplace_back(offset + i * 2 + 2);

        tris.emplace_back(offset + i * 2 + 3);
        tris.emplace_back(offset + i * 2 + 2);
        tris.emplace_back(offset + i * 2 + 1);
    }

    // hasty check that indices use all vertices and don't overshoot
    assert(*std::max_element(tris.begin(), tris.end()) == verts.size() - 1);

    *triangles = std::move(tris);
    *vertices = std::move(verts);

    if (normals) {
        *normals = std::move(norms);
    }
    if (tex_coords) {
        *tex_coords = std::move(tcoords);
    }
}

void build_sphere_mesh(unsigned divisions,
                       std::vector<unsigned> *triangles,
                       std::vector<glm::vec3> *vertices,
                       std::vector<glm::vec3> *normals,
                       std::vector<glm::vec2> *tex_coords)
{
    assert(triangles);
    assert(vertices);
    divisions = std::max(4u, divisions);

    std::vector<unsigned> tris;
    std::vector<glm::vec3> verts;
    std::vector<glm::vec3> norms;
    std::vector<glm::vec2> tcoords;

    auto half_divs = divisions / 2;

    // Unit Sphere:
    // p = {
    //      cos(θ) * sin(ϕ),
    //      sin(θ) * sin(ϕ),
    //      cos(ϕ)
    //     }

    // outer loop sets the horizontal θ angle
    for (unsigned i = 0; i <= divisions; ++i) {
        float u = i * 1.f / divisions;
        float theta = u * glm::two_pi<float>();
        glm::vec2 p2(std::cos(theta), std::sin(theta));

        // outer loop sets the vertical ϕ angle
        for (unsigned j = 0; j <= half_divs; ++j) {
            float v = j * 1.f / half_divs;
            float phi = v * glm::pi<float>();

            glm::vec3 n(p2 * std::sin(phi), std::cos(phi));
            glm::vec3 p = n * 0.5f;

            verts.emplace_back(p);
            norms.emplace_back(n);
            tcoords.emplace_back(u, v);
        }
    }

    // create individual triangles in a strip like fashion (so not actually a triangle strip)
    unsigned index = 0;
    for (unsigned i = 0; i < divisions; ++i) {
        for (unsigned j = 0; j <= half_divs; ++j) {
            tris.emplace_back(index + half_divs);
            tris.emplace_back(index);
            tris.emplace_back(index + half_divs + 1);

            tris.emplace_back(index + 1);
            tris.emplace_back(index + half_divs + 1);
            tris.emplace_back(index);
            ++index;
        }
    }

    // hasty check that indices use all vertices and don't overshoot
    assert(*std::max_element(tris.begin(), tris.end()) == verts.size() - 1);

    *triangles = std::move(tris);
    *vertices = std::move(verts);

    if (normals) {
        *normals = std::move(norms);
    }
    if (tex_coords) {
        *tex_coords = std::move(tcoords);
    }
}

void build_torus_mesh(unsigned divisions,
                      float max_radius,
                      float inner_radius,
                      std::vector<unsigned> *triangles,
                      std::vector<glm::vec3> *vertices,
                      std::vector<glm::vec3> *normals,
                      std::vector<glm::vec2> *tex_coords)
{
    assert(triangles);
    assert(vertices);
    divisions = std::max(3u, divisions);

    std::vector<unsigned> tris;
    std::vector<glm::vec3> verts;
    std::vector<glm::vec3> norms;
    std::vector<glm::vec2> tcoords;

    float outer_radius = max_radius - inner_radius;

    // outer loop sets the horizontal angle
    for (unsigned i = 0; i <= divisions; ++i) {
        float u = i * 1.f / divisions;
        float theta = u * glm::two_pi<float>();
        auto outer_dir = glm::vec3(std::cos(theta), std::sin(theta), 0.f);

        // outer loop sets the vertical angle
        for (unsigned j = 0; j <= divisions; ++j) {
            float v = j * 1.f / divisions;
            float phi = v * glm::two_pi<float>();

            glm::vec3 n = outer_dir * std::cos(phi) + glm::vec3(0.f, 0.f, std::sin(phi));
            glm::vec3 p = outer_dir * outer_radius + n * inner_radius;

            verts.emplace_back(p);
            norms.emplace_back(n);
            tcoords.emplace_back(u, v);
        }
    }

    // create individual triangles in a strip like fashion (so not actually a triangle strip)
    unsigned index = 0;
    for (unsigned i = 0; i < divisions; ++i) {
        for (unsigned j = 0; j <= divisions; ++j) {
            tris.emplace_back(index + divisions);
            tris.emplace_back(index + divisions + 1);
            tris.emplace_back(index);

            tris.emplace_back(index + 1);
            tris.emplace_back(index);
            tris.emplace_back(index + divisions + 1);
            ++index;
        }
    }

    // hasty check that indices use all vertices and don't overshoot
    assert(*std::max_element(tris.begin(), tris.end()) == verts.size() - 1);

    *triangles = std::move(tris);
    *vertices = std::move(verts);

    if (normals) {
        *normals = std::move(norms);
    }
    if (tex_coords) {
        *tex_coords = std::move(tcoords);
    }
}

} // namespace glc
