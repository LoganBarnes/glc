// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include "glc/scene/StaticScene.hpp"
#include <glc/camera/Camera.hpp>
#include <glc/Program.hpp>
#include <glc/VertexArray.hpp>
#include <glc/GLCPaths.hpp>
#include <random>

namespace glc {

namespace {

// turns position, normal, tex coord, and vertex color vectors into a single char vector
// so all the data can be added to a single VBO
template <typename T>
void create_byte_data(std::vector<char> &byte_data_out, const std::vector<T> &data)
{
    auto byte_size = data.size() * sizeof(T);
    auto copy_start = byte_data_out.size();
    byte_data_out.resize(copy_start + byte_size);
    std::memcpy(byte_data_out.data() + copy_start, data.data(), byte_size);
}
} // namespace

namespace detail {

void check_error_and_update_flags(unsigned &value, glc::detail::ParamType::Type flag)
{
    static std::unordered_map<unsigned, std::string> error_msgs = {
        {glc::detail::ParamType::POSITIONS, "Positions vector"},
        {glc::detail::ParamType::NORMALS, "Normals vector"},
        {glc::detail::ParamType::TEX_COORDS, "TexCoords vector"},
        {glc::detail::ParamType::VERTEX_COLORS, "Colors vector"},
        {glc::detail::ParamType::INDICES, "Indices vector"},
        {glc::detail::ParamType::DISPLAY_MODE, "DisplayMode"},
        {glc::detail::ParamType::TRANSFORM, "Transform"},
        {glc::detail::ParamType::PARENT, "Parent UUID"},
        {glc::detail::ParamType::READABLE_ID, "Readable ID"},
        {glc::detail::ParamType::VISIBLE, "Visible boolean"},
        {glc::detail::ParamType::CHILDREN_VISIBLE, "Children Visible boolean"},
        {glc::detail::ParamType::GLOBAL_COLOR, "Global color"},
    };
    if (value & flag) {
        throw std::runtime_error(error_msgs.at(flag) + " provided more than once");
    }
    value |= flag;
}
} // namespace detail

glc::StaticScene::StaticScene()
    : program_(glc::create_program(glc::shader_path() + "default.vert", glc::shader_path() + "default.frag"))
{
    items_.emplace(nullptr, std::make_unique<detail::Item>());
}

void glc::StaticScene::render(const glc::Camera &camera) const
{
    program_->use([&] {
        program_->set_uniform("clip_from_world", camera.get_clip_from_world_matrix());
        program_->set_uniform("world_eye_position", camera.get_eye_pos());
        if (lights_ssbo_) {
            program_->set_ssbo_uniform("light_data", lights_ssbo_, 0);
        }

        const auto &root = items_.at(nullptr);

        for (const auto &child_uuid : root->children) {
            const auto &child = items_.at(child_uuid);
            render_item(*child, camera, root->info.transform);
        }
    });
}

bool StaticScene::remove_item(uid uuid, bool remove_children)
{
    if (uuid == nullptr) {
        std::cerr << "WARNING: Can't delete an item with a nil uid" << std::endl;
        return false;
    }
    if (items_.find(uuid) == items_.end()) {
        std::cerr << "WARNING: Item with uid does not exist" << std::endl;
        return false;
    }
    {
        // all items have a parent because there is a root node (nil uuid) in the scene
        detail::Item &item = *items_.at(uuid);
        detail::Item &parent = *items_.at(item.info.parent);

        if (remove_children) {
            for (auto &child : item.children) {
                remove_item_and_children(child);
            }
        } else {
            for (auto &child : item.children) {
                detail::Item &child_item = *items_.at(child);
                // add parent transform to child so the item doesn't move
                child_item.info.transform = item.info.transform * child_item.info.transform;
                // reset parent of children
                child_item.info.parent = item.info.parent;
                // add children to current parent
                parent.children.emplace(child);
            }
        }
        // remove current item from parent
        parent.children.erase(uuid);
    }
    items_.erase(uuid);
    return true;
}

// update the cpu light vector and the gpu light buffer
void StaticScene::add_light(glm::vec3 position, float intensity)
{
    lights_.emplace_back(position, intensity);
    lights_ssbo_ = glc::create_buffer<GL_SHADER_STORAGE_BUFFER>(lights_);
}

const std::vector<glm::vec4> &StaticScene::get_lights() const
{
    return lights_;
}

const glc::Buffer<glm::vec4> &StaticScene::get_light_ssbo() const
{
    return lights_ssbo_;
}

bool StaticScene::has_item(uid uuid) const
{
    return items_.find(uuid) != items_.end();
}

// set all the per item uniforms, render the item, and recusively call this function on all children
void StaticScene::render_item(const detail::Item &item,
                              const glc::Camera &camera,
                              const glm::mat4 &parent_transform) const
{
    const UpdatableInfo &info = item.info;
    glm::mat4 trans = parent_transform * info.transform;

    if (info.visible) {
        if (item.renderable) {
            item.renderable->on_render(camera, info, *this, trans);
            glUseProgram(program_->get_id());
        } else {
            const glc::Pipeline<char> &pl = item.pl;

            glm::mat3 normal_trans = glm::inverse(glm::transpose(glm::mat3(trans)));

            program_->set_uniform("display_mode", static_cast<int>(info.shader_display_mode));
            program_->set_uniform("world_from_local", trans);
            program_->set_uniform("world_from_local_normals", normal_trans);
            program_->set_uniform("shape_color", info.color);

            pl.vao->render(info.gl_draw_mode, 0, pl.draw_size, pl.ibo);
        }
    }

    for (const auto &child_uuid : item.children) {
        const auto &child = items_.at(child_uuid);
        render_item(*child, camera, trans);
    }
}

uid StaticScene::actually_add_item(const detail::ItemData &item_data)
{
    if (!(item_data.added_fields & (detail::ParamType::POSITIONS | detail::ParamType::RENDERABLE))) {
        throw std::runtime_error("No position vector or renderable class provided");
    }

    auto item = std::make_unique<detail::Item>();

    if (item_data.added_fields & detail::ParamType::RENDERABLE) {
        assert(item_data.renderable);

        auto mesh_stuff_flags = (detail::ParamType::POSITIONS | detail::ParamType::NORMALS
                                 | detail::ParamType::TEX_COORDS | detail::ParamType::VERTEX_COLORS);

        if ((item_data.added_fields & mesh_stuff_flags)) {
            std::cerr << "WARNING: Renderable class specified; "
                      << "ignoring all positions, normals, tex coords, and color params" << std::endl;
        }

        item->renderable = item_data.renderable;

    } else {
        assert(item_data.renderable == nullptr);

        const std::size_t pos_size = item_data.vao_elements.at(0).vector_size;
        float *offset = nullptr;

        std::vector<glc::VAOElement> vao_elements;

        for (const auto &element : item_data.vao_elements) {

            if (pos_size != element.vector_size) {
                throw std::runtime_error("All Positions, Normals, TexCoords, and Colors arrays must be the same size");
            }

            vao_elements.emplace_back(element.name, element.element_size, GL_FLOAT, offset);
            offset += element.vector_size * static_cast<unsigned>(element.element_size);
        }

        item->pl.vbo = glc::create_buffer(item_data.vertex_data);
        item->pl.vao = glc::create_vertex_array(program_, item->pl.vbo, 0, vao_elements);

        if (!item_data.index_data.empty()) {
            item->pl.ibo = glc::create_buffer<GL_ELEMENT_ARRAY_BUFFER>(item_data.index_data);
            item->pl.draw_size = static_cast<int>(item_data.index_data.size());
        } else {
            item->pl.draw_size = static_cast<int>(pos_size);
        }
    }

    item->info = item_data.info;

    if (items_.find(item->info.parent) == items_.end()) {
        throw std::runtime_error("Parent uuid does not exist");
    }

    auto uuid = item.get();

    items_.at(item->info.parent)->children.emplace(uuid);
    items_.emplace(uuid, std::move(item));

    return uuid;
}

void StaticScene::remove_item_and_children(uid uuid)
{
    detail::Item &item = *items_.at(uuid);
    for (auto &child : item.children) {
        remove_item_and_children(child);
    }
    items_.erase(uuid);
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantPositions &positions)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::POSITIONS);
    create_byte_data(item_data.vertex_data, positions.data);
    item_data.vao_elements.emplace_back("local_position", 3, positions.data.size());
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantNormals &normals)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::NORMALS);
    create_byte_data(item_data.vertex_data, normals.data);
    item_data.vao_elements.emplace_back("local_normal", 3, normals.data.size());
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantTexCoords &texCoords)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::TEX_COORDS);
    create_byte_data(item_data.vertex_data, texCoords.data);
    item_data.vao_elements.emplace_back("tex_coords", 2, texCoords.data.size());
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantVertexColors &colors)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::VERTEX_COLORS);
    create_byte_data(item_data.vertex_data, colors.data);
    item_data.vao_elements.emplace_back("color", 3, colors.data.size());
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantDisplayType &display_type)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::DISPLAY_MODE);
    item_data.info.shader_display_mode = display_type.data;
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantTransform &transform)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::TRANSFORM);
    item_data.info.transform = transform.data;
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantParent &parent)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::PARENT);
    item_data.info.parent = parent.data;
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantReadableID &readable_id)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::READABLE_ID);
    item_data.info.readable_id = readable_id.data;
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantVisible &visible)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::VISIBLE);
    item_data.info.visible = visible.data;
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const ConstantGlobalColor &global_color)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::GLOBAL_COLOR);
    item_data.info.color = global_color.data;
}

void StaticScene::detail_add_item(detail::ItemData &item_data, const Renderable &renderable)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::RENDERABLE);
    item_data.renderable = renderable.data;
}

void StaticScene::detail_update_item(unsigned added_fields, detail::Item &item, const ConstantDisplayType &display_type)
{
    detail::check_error_and_update_flags(added_fields, detail::ParamType::DISPLAY_MODE);
    item.info.shader_display_mode = display_type.data;
}

void StaticScene::detail_update_item(unsigned added_fields, detail::Item &item, const ConstantTransform &transform)
{
    detail::check_error_and_update_flags(added_fields, detail::ParamType::TRANSFORM);
    item.info.transform = transform.data;
}

void StaticScene::detail_update_item(unsigned added_fields, detail::Item &item, const ConstantReadableID &readable_id)
{
    detail::check_error_and_update_flags(added_fields, detail::ParamType::READABLE_ID);
    item.info.readable_id = readable_id.data;
}

void StaticScene::detail_update_item(unsigned added_fields, detail::Item &item, const ConstantVisible &visible)
{
    detail::check_error_and_update_flags(added_fields, detail::ParamType::VISIBLE);
    item.info.visible = visible.data;
}

void StaticScene::detail_update_item(unsigned added_fields,
                                     detail::Item &item,
                                     const ConstantChildrenVisible &children_visible)
{
    detail::check_error_and_update_flags(added_fields, detail::ParamType::CHILDREN_VISIBLE);
    for (auto &child : item.children) {
        auto &child_item = *items_.at(child);
        detail_update_item(detail::ParamType::NONE, child_item, glc::Visible(children_visible.data));
        detail_update_item(detail::ParamType::NONE, child_item, glc::ChildrenVisible(children_visible.data));
    }
}

void StaticScene::detail_update_item(unsigned added_fields, detail::Item &item, const ConstantGlobalColor &global_color)
{
    detail::check_error_and_update_flags(added_fields, detail::ParamType::GLOBAL_COLOR);
    item.info.color = global_color.data;
}

void StaticScene::detail_get_item_info(const detail::Item &item, UpdatableDisplayType &display_type)
{
    *display_type.data = item.info.shader_display_mode;
}

void StaticScene::detail_get_item_info(const detail::Item &item, UpdatableTransform &transform)
{
    *transform.data = item.info.transform;
}

void StaticScene::detail_get_item_info(const detail::Item &item, UpdatableReadableID &readable_id)
{
    *readable_id.data = item.info.readable_id;
}

void StaticScene::detail_get_item_info(const detail::Item &item, UpdatableVisible &visible)
{
    *visible.data = item.info.visible;
}

void StaticScene::detail_get_item_info(const detail::Item &item, UpdatableGlobalColor &global_color)
{
    *global_color.data = item.info.color;
}

} // namespace glc
