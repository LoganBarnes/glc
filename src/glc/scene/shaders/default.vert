// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#version 410 core
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 local_position;
layout(location = 1) in vec3 local_normal;
layout(location = 2) in vec2 tex_coords;

uniform mat4 clip_from_world = mat4(1.f);
uniform mat4 world_from_local = mat4(1.f);
uniform mat3 world_from_local_normals = mat3(1.f);

layout(location = 0) out Vertex
{
    vec3 world_position;
    vec3 world_normal;
    vec2 tex_coords;
} vertex;

out gl_PerVertex
{
  vec4 gl_Position;
};

void main(void)
{
    vertex.world_position = vec3(world_from_local * vec4(local_position, 1.f));
    vertex.world_normal   = world_from_local_normals * local_normal;
    vertex.tex_coords = tex_coords;

    gl_Position = clip_from_world * vec4(vertex.world_position, 1.f);
}
