// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#version 450 core
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in Vertex
{
    vec3 world_position;
    vec3 world_normal;
    vec2 tex_coords;
} vertex;

const float PI = 3.141592653589793f;

const int DISPLAY_MODE_POSITION = 0;
const int DISPLAY_MODE_NORMALS = 1;
const int DISPLAY_MODE_TEX_COORDS = 2;
const int DISPLAY_MODE_COLOR = 3;
const int DISPLAY_MODE_TEXTURE = 4;
const int DISPLAY_MODE_SIMPLE_SHADING = 5;
const int DISPLAY_MODE_ADVANCED_SHADING = 6;
const int DISPLAY_MODE_WHITE = 7;

layout(std430, binding = 0) buffer light_data
{
    vec4 lights[];
};
uniform int  display_mode = 2;
uniform vec3 shape_color = vec3(1.f);
uniform float alpha = 1.f;
uniform sampler2D tex;
uniform vec3 light_dir = normalize(-vec3(0.7f, 0.85f, 1.f));

uniform float roughness = 0.2f;
uniform vec3  index_of_refraction = vec3(1.5145f, 1.5208f, 1.5232f);
uniform vec3  world_eye_position;

layout(location = 0) out vec4 out_color;

////////////non-polaraized and non-magnetic////////////
float fresnel(in  vec3  incident,
              in  vec3  normal,
              in  float n1, // refractive index of current medium
              in  float n2, // refractive index of material
              out vec3  reflection,
              out vec3  transmission)
{
    reflection = reflect(-incident, normal);
    transmission = refract(-incident, normal, n1 / n2);

    float cosI = dot(incident, normal);
    float cosT = dot(transmission, -normal);

    float n1CosI = n1 * cosI;
    float n2CosT = n2 * cosT;

    float n1CosT = n1 * cosT;
    float n2CosI = n2 * cosI;

    float Rs = (n1CosI - n2CosT) / (n1CosI + n2CosT);
    Rs *= Rs;

    float Rp = (n1CosT - n2CosI) / (n1CosT + n2CosI);
    Rp *= Rp;

    return 0.5 * (Rs + Rp);
}

////////////Cook-Torrance////////////
vec3 calcBRDF(in vec3 to_eye,
              in vec3 normal,
              in vec3 to_light)
{
    // roughness == m in cook-torrance lingo
    float m = roughness;

    vec3 R, T;
    vec3 F = vec3(fresnel(to_eye, normal, 1.f, index_of_refraction.x, R, T),
                  fresnel(to_eye, normal, 1.f, index_of_refraction.y, R, T),
                  fresnel(to_eye, normal, 1.f, index_of_refraction.z, R, T));

    vec3 half_vector = normalize(to_eye + to_light);

    float cosNV = dot(normal, to_eye);
    float cosNH = dot(normal, half_vector);
    float cosNL = dot(normal, to_light);
    float cosVH = dot(to_eye, half_vector);

    // geometric attenuation
    float G = min(1.f, min(2.f * cosNH * cosNV / cosVH, 2.f * cosNH * cosNL / cosVH));

    // microfacet slope distribution
    float cosNHPow2 = cosNH * cosNH;
    float mPow2 = m * m;

    float D = (1.f / (PI * mPow2 * cosNHPow2 * cosNHPow2))
              * exp((cosNHPow2 - 1.f) / (mPow2 * cosNHPow2));

    vec3 specular = shape_color * (F * D * G) / (PI * cosNL * cosNV);
    vec3 diffuse = shape_color * (1.f - F) / PI;

    return diffuse + specular;
}


void main(void)
{
    vec3 world_normal = normalize(vertex.world_normal);

    vec3 color = vec3(1.f);

    switch(display_mode)
    {
    case DISPLAY_MODE_POSITION:
        color = vertex.world_position;
        break;
    case DISPLAY_MODE_NORMALS:
        color = world_normal * 0.5f + 0.5f;
        break;
    case DISPLAY_MODE_TEX_COORDS:
        color = vec3(vertex.tex_coords, 1.f);
        break;
    case DISPLAY_MODE_COLOR:
        color = shape_color;
        break;
    case DISPLAY_MODE_TEXTURE:
        color = texture(tex, vertex.tex_coords).rgb;
        break;
    case DISPLAY_MODE_SIMPLE_SHADING:
        {
            float ambient = 0.1f;
            float intensity = max(ambient, dot(world_normal, -light_dir));
            color = shape_color * intensity;
        }
        break;
    case DISPLAY_MODE_ADVANCED_SHADING:
        {
            vec3 point_to_eye = normalize(world_eye_position - vertex.world_position);

            vec3 intensity = shape_color * 0.1f; // ambient :(

            for (int i = 0; i < lights.length(); ++i)
            {
                vec3 light_pos = lights[i].xyz;
                float light_intensity = lights[i].w;

                vec3 point_to_light = light_pos - vertex.world_position;
                vec3 w_l = normalize(point_to_light);

                if (dot(world_normal, w_l) > 0.f) {
                    intensity += calcBRDF(point_to_eye, world_normal, w_l)
                                 * light_intensity / dot(point_to_light, point_to_light)
                                 * max(0.f, dot(world_normal, w_l));
                }
            }
            color = intensity;
        }
        break;
    case DISPLAY_MODE_WHITE:
    default:
        break;
    }

    out_color = vec4(color, alpha);
}
