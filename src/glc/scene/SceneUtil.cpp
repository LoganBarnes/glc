// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include "glc/scene/SceneUtil.hpp"
#include "glc/scene/StaticScene.hpp"
#include "glc/PrimitiveMeshes.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

namespace glc {

namespace {

constexpr int subdivisions = 100;

void add_axis(
    glc::StaticScene &scene, const std::string &id, const glm::vec3 &color, const glm::mat4 &rotation, uid root)
{

    std::vector<unsigned> indices;
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    // Scale the cylinder by 7 in the Z direction then move it up so the bottom is at Z = 0.0
    glm::mat4 trans = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, 3.5f))
        * glm::scale(glm::mat4(1.f), glm::vec3(1.f, 1.f, 7.f));

    glc::build_cylinder_mesh(subdivisions, &indices, &vertices, &normals);

    auto cyl_id = scene.add_item(glc::Triangles(indices),
                                 glc::Positions(vertices),
                                 glc::Normals(normals),
                                 glc::Transform(rotation * trans),
                                 glc::DisplayType(glc::DisplayMode::ADVANCED_SHADING),
                                 glc::ReadableID(id),
                                 glc::GlobalColor(color),
                                 glc::Parent(root));

    glc::build_cone_mesh(subdivisions, &indices, &vertices, &normals);

    // Scale the cone by 2 around the base and shrink it by 0.3f in Z. Then move it up so the cone
    // bottom is at 0.5.
    //
    // NOTE: since this cone is a child of the above cylinder the cylinder's transformations will
    // propagate to this shape. All these transforms are relative to a the unit cylinder before
    // it is transformed.
    glm::mat4 local_trans = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, 0.5f + 0.15f))
        * glm::scale(glm::mat4(1.f), glm::vec3(2.f, 2.f, 0.3f));

    scene.add_item(glc::Triangles(indices),
                   glc::Positions(vertices),
                   glc::Normals(normals),
                   glc::Transform(local_trans),
                   glc::Parent(cyl_id),
                   glc::DisplayType(glc::DisplayMode::ADVANCED_SHADING),
                   glc::ReadableID(id + " arrowhead"),
                   glc::GlobalColor(color));
}

} // namespace

uid glc::SceneUtil::add_axes_at_origin(glc::StaticScene &scene, float scale)
{
    uid root = scene.add_item(glc::Positions(),
                              glc::Transform(glm::scale(glm::mat4(1.f), glm::vec3(scale))),
                              glc::ReadableID("Axes"));

    std::vector<unsigned> indices;
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    glc::build_sphere_mesh(subdivisions, &indices, &vertices, &normals);

    scene.add_item(glc::Triangles(indices),
                   glc::Positions(vertices),
                   glc::Normals(normals),
                   glc::Transform(glm::scale(glm::mat4(1.f), glm::vec3(1.5f))),
                   glc::DisplayType(glc::DisplayMode::ADVANCED_SHADING),
                   glc::ReadableID("Axes"),
                   glc::GlobalColor({1.f, 1.f, 1.f}),
                   glc::Parent(root));

    glm::mat4 rotation;

    rotation = glm::mat4(1.f);
    add_axis(scene, "Z", glm::vec3(0.f, 0.f, 1.f), rotation, root);

    rotation = glm::rotate(glm::mat4(1.f), glm::half_pi<float>(), glm::vec3(-1, 0, 0));
    add_axis(scene, "Y", glm::vec3(0.f, 1.f, 0.f), rotation, root);

    rotation = glm::rotate(glm::mat4(1.f), glm::half_pi<float>(), glm::vec3(0, 1, 0));
    add_axis(scene, "X", glm::vec3(1.f, 0.f, 0.f), rotation, root);

    return root;
}

} // namespace glc
