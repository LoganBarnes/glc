// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/Program.hpp>
#include <glc/Texture.hpp>
#include "detail/ProgramManager.hpp"
#include <sstream>
#include <glm/gtc/type_ptr.hpp>

namespace glc {

namespace detail {

namespace {

std::shared_ptr<GLuint> create_program(const IdVec &shader_ids)
{
    // capture a copy of shader_ids so they aren't deleted until the program is deleted
    std::shared_ptr<GLuint> program_id(new GLuint(glCreateProgram()), [shader_ids](auto *id) {
        glDeleteProgram(*id);
        delete id;
    });

    GLuint program = *program_id;

    for (auto &shader_id : shader_ids) {
        glAttachShader(program, *shader_id);
    }

    glLinkProgram(program);

    // Check program
    GLint result = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &result);

    if (result == GL_FALSE) {
        int log_length = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);
        std::vector<char> program_error(static_cast<std::size_t>(log_length));
        glGetProgramInfoLog(program, log_length, nullptr, program_error.data());

        // shaders and programs get deleted when shared_ptr goes out of scope
        throw std::runtime_error("(Program) " + std::string(program_error.data()));
    }

    for (auto &shader_id : shader_ids) {
        glDetachShader(program, *shader_id);
    }

    return program_id;
} // create_program

} // namespace

ProgramWrapper::ProgramWrapper(const std::vector<std::string> &shader_filenames)
{
    IdVec shaders;

    for (const std::string &filename : shader_filenames) {
        shaders.emplace_back(create_shader(filename));
    }

    program_ = create_program(shaders);
}

ProgramWrapper::ProgramWrapper(const std::string &separable_shader, GLenum shader_type)
{
    // Load shader
    std::string shader_str = read_file(separable_shader);
    const char *shader_source = shader_str.c_str();

    program_
        = std::shared_ptr<GLuint>(new GLuint(glCreateShaderProgramv(shader_type, 1, &shader_source)), [](auto *id) {
              glDeleteProgram(*id);
              delete id;
          });

    // Check program
    GLint result = GL_FALSE;
    glGetProgramiv(get_id(), GL_LINK_STATUS, &result);

    if (result == GL_FALSE) {
        int log_length = 0;
        glGetProgramiv(get_id(), GL_INFO_LOG_LENGTH, &log_length);
        std::vector<char> program_error(static_cast<std::size_t>(log_length));
        glGetProgramInfoLog(get_id(), log_length, nullptr, program_error.data());

        // program deleted when shared_ptr goes out of scope
        throw std::runtime_error("(SeparableShaderProgram) " + std::string(program_error.data()));
    }
}

bool ProgramWrapper::set_int_uniform_array(const std::string &uniform,
                                           const int *value,
                                           const int size,
                                           const int count) const
{
    auto location = glGetUniformLocation(get_id(), uniform.c_str());
    switch (size) {
    case 1:
        glProgramUniform1iv(get_id(), location, count, value);
        break;
    case 2:
        glProgramUniform2iv(get_id(), location, count, value);
        break;
    case 3:
        glProgramUniform3iv(get_id(), location, count, value);
        break;
    case 4:
        glProgramUniform4iv(get_id(), location, count, value);
        break;
    default: {
        std::stringstream msg;
        msg << "ivec" << size << " uniform does not exist";
        throw std::runtime_error(msg.str());
    }
    } // switch
    return location != -1;
}

bool ProgramWrapper::set_float_uniform_array(const std::string &uniform,
                                             const float *value,
                                             const int size,
                                             const int count) const
{
    auto location = glGetUniformLocation(get_id(), uniform.c_str());
    switch (size) {
    case 1:
        glProgramUniform1fv(get_id(), location, count, value);
        break;
    case 2:
        glProgramUniform2fv(get_id(), location, count, value);
        break;
    case 3:
        glProgramUniform3fv(get_id(), location, count, value);
        break;
    case 4:
        glProgramUniform4fv(get_id(), location, count, value);
        break;
    default: {
        std::stringstream msg;
        msg << "vec" << size << " uniform does not exist";
        throw std::runtime_error(msg.str());
    }
    } // switch
    return location != -1;
}

bool ProgramWrapper::set_matrix_uniform_array(const std::string &uniform,
                                              const float *value,
                                              const int size,
                                              const int count) const
{
    auto location = glGetUniformLocation(get_id(), uniform.c_str());
    switch (size) {
    case 2:
        glProgramUniformMatrix2fv(get_id(), location, count, GL_FALSE, value);
        break;
    case 3:
        glProgramUniformMatrix3fv(get_id(), location, count, GL_FALSE, value);
        break;
    case 4:
        glProgramUniformMatrix4fv(get_id(), location, count, GL_FALSE, value);
        break;
    default: {
        std::stringstream msg;
        msg << "mat" << size << " uniform does not exist";
        throw std::runtime_error(msg.str());
    }
    } // switch
    return location != -1;
}

bool ProgramWrapper::set_uniform(const std::string &uniform, const bool value) const
{
    auto location = glGetUniformLocation(get_id(), uniform.c_str());
    glProgramUniform1i(get_id(), location, value);
    return location != -1;
}

bool ProgramWrapper::set_uniform(const std::string &uniform, int val) const
{
    return set_int_uniform_array(uniform, &val);
}

bool ProgramWrapper::set_uniform(const std::string &uniform, float val) const
{
    return set_float_uniform_array(uniform, &val);
}

bool ProgramWrapper::set_uniform(const std::string &uniform, const Texture &texture, int active_tex) const
{
    auto location = glGetUniformLocation(get_id(), uniform.c_str());
    glActiveTexture(static_cast<GLenum>(GL_TEXTURE0 + active_tex));
    glProgramUniform1i(get_id(), location, active_tex);
    texture->bind();
    return location != -1;
}

GLuint ProgramWrapper::get_id() const
{
    return *program_;
}

} // namespace detail

Program create_program(const std::vector<std::string> &shader_filenames)
{
    return detail::ProgramManager::create_program(shader_filenames);
}

} // namespace glc
