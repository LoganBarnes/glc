// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include "ProgramManager.hpp"
#include <glc/Program.hpp>
#include <fstream>

namespace glc {

namespace detail {

namespace {

glc::Program check_for_program(std::unordered_map<std::string, std::weak_ptr<detail::ProgramWrapper>> &programs,
                               const std::string &key)
{
    auto iter = programs.find(key); // long string hashes may be inefficient so we only do this once
    if (iter != programs.end()) {
        if (auto program = iter->second.lock()) { // check weak ptr
            return program;
        }
        programs.erase(iter);
    }
    return nullptr;
}

} // namespace

glc::Program ProgramManager::create_program(const std::vector<std::string> &shader_filenames)
{
    return ProgramManager::instance().detail_create_program(shader_filenames);
}

glc::Program ProgramManager::create_separable_program(const std::string &shader_filename, GLenum shader_type)
{
    return ProgramManager::instance().detail_create_separable_program(shader_filename, shader_type);
}

ProgramManager &ProgramManager::instance()
{
    static ProgramManager pm;
    return pm;
}

glc::Program ProgramManager::detail_create_program(const std::vector<std::string> &shader_filenames)
{
    std::string key;
    for (const std::string &filename : shader_filenames) {
        key += filename;
    }

    if (auto program = check_for_program(programs_, key)) {
        return program;
    }

    glc::Program program = std::make_shared<detail::ProgramWrapper>(shader_filenames);
    programs_.emplace(key, program);

    return program;
}

glc::Program ProgramManager::detail_create_separable_program(const std::string &shader_filename, GLenum shader_type)
{
    if (auto program = check_for_program(separable_programs_, shader_filename)) {
        return program;
    }

    glc::Program program = std::make_shared<detail::ProgramWrapper>(shader_filename, shader_type);
    separable_programs_.emplace(shader_filename, program);

    return program;
}

const std::unordered_map<std::string, GLenum> &shader_types()
{
    static std::unordered_map<std::string, GLenum> ext_map{{".vert", GL_VERTEX_SHADER},
                                                           {".tesc", GL_TESS_CONTROL_SHADER},
                                                           {".tese", GL_TESS_EVALUATION_SHADER},
                                                           {".geom", GL_GEOMETRY_SHADER},
                                                           {".frag", GL_FRAGMENT_SHADER},
                                                           {".comp", GL_COMPUTE_SHADER}};
    return ext_map;
}

const std::unordered_map<GLenum, std::string> &shader_type_strings()
{
    static std::unordered_map<GLenum, std::string> type_map{{GL_VERTEX_SHADER, "GL_VERTEX_SHADER"},
                                                            {GL_TESS_CONTROL_SHADER, "GL_TESS_CONTROL_SHADER"},
                                                            {GL_TESS_EVALUATION_SHADER, "GL_TESS_EVALUATION_SHADER"},
                                                            {GL_GEOMETRY_SHADER, "GL_GEOMETRY_SHADER"},
                                                            {GL_FRAGMENT_SHADER, "GL_FRAGMENT_SHADER"},
                                                            {GL_COMPUTE_SHADER, "GL_COMPUTE_SHADER"}};
    return type_map;
}

std::string read_file(const std::string &filename)
{
    std::ifstream file(filename, std::ios::in);

    if (!file.is_open() || !file.good()) {
        throw std::runtime_error("Could not read file: " + filename);
    }

    // get full file size
    file.seekg(0, std::ios::end);
    size_t size = static_cast<size_t>(file.tellg());

    // allocate string of correct size
    std::string buffer(size, ' ');

    // fill string with file contents
    // note: string memory is only guaranteed contiguous in C++11 and up
    file.seekg(0);
    file.read(&buffer[0], static_cast<std::streamsize>(size));

    file.close();

    return buffer;
}

std::shared_ptr<GLuint> create_shader(GLenum shader_type, const std::string &filename)
{
    std::shared_ptr<GLuint> shader_id(new GLuint(glCreateShader(shader_type)), [](auto *id) {
        glDeleteShader(*id);
        delete id;
    });

    GLuint shader = *shader_id;

    // Load shader
    std::string shader_str = read_file(filename);
    const char *shader_source = shader_str.c_str();

    // Compile shader
    glShaderSource(shader, 1, &shader_source, nullptr);
    glCompileShader(shader);

    // Check shader
    GLint result = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);

    if (result == GL_FALSE) {
        int log_length = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);
        std::vector<char> shader_error(static_cast<size_t>(log_length));
        glGetShaderInfoLog(shader, log_length, nullptr, shader_error.data());

        // shader will get deleted when shared_ptr goes out of scope
        throw std::runtime_error("(" + shader_type_strings().at(shader_type) + ") " + std::string(shader_error.data()));
    }
    return shader_id;
}

std::shared_ptr<GLuint> create_shader(const std::string &filename)
{
    size_t dot = filename.find_last_of(".");
    std::string ext = filename.substr(dot);

    if (shader_types().find(ext) == shader_types().end()) {
        throw std::runtime_error("Unknown shader extension: " + ext);
    }

    return create_shader(shader_types().at(ext), filename);
}

} // namespace detail

} // namespace glc
