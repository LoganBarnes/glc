// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>
#include <unordered_map>
#include <vector>

namespace glc {

namespace detail {

class ProgramManager
{
public:
    static glc::Program create_program(const std::vector<std::string> &shader_filenames);
    static glc::Program create_separable_program(const std::string &shader_filename, GLenum shader_type);

    static ProgramManager &instance();

    ProgramManager(const ProgramManager &) = delete;
    ProgramManager(ProgramManager &&) noexcept = delete;

    ProgramManager &operator=(const ProgramManager &) = delete;
    ProgramManager &operator=(ProgramManager &&) noexcept = delete;

private:
    ProgramManager() = default;

    glc::Program detail_create_program(const std::vector<std::string> &shader_filenames);
    glc::Program detail_create_separable_program(const std::string &shader_filename, GLenum shader_type);

    std::unordered_map<std::string, std::weak_ptr<detail::ProgramWrapper>> programs_;
    std::unordered_map<std::string, std::weak_ptr<detail::ProgramWrapper>> separable_programs_;
};

using IdVec = std::vector<std::shared_ptr<GLuint>>;

const std::unordered_map<std::string, GLenum> &shader_types();
const std::unordered_map<GLenum, std::string> &shader_type_strings();

std::string read_file(const std::string &filename);

std::shared_ptr<GLuint> create_shader(GLenum shader_type, const std::string &filename);
std::shared_ptr<GLuint> create_shader(const std::string &filename);

} // namespace detail

} // namespace glc
