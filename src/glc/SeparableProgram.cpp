// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/SeparableProgram.hpp>
#include <glc/detail/ProgramManager.hpp>

namespace glc {

namespace detail {

SeparableProgramWrapper::SeparableProgramWrapper(const std::vector<std::string> &shader_filenames)
{
    for (const auto &filename : shader_filenames) {
        size_t dot = filename.find_last_of('.');
        std::string ext = filename.substr(dot);

        if (glc::detail::shader_types().find(ext) == glc::detail::shader_types().end()) {
            throw std::runtime_error("Unknown shader extension: " + ext);
        }

        GLenum type = glc::detail::shader_types().at(ext);
        glc::Program program = ProgramManager::create_separable_program(filename, type);

        switch (type) {
        case GL_VERTEX_SHADER:
            vert_ = std::move(program);
            break;
        case GL_TESS_CONTROL_SHADER:
            tesc_ = std::move(program);
            break;
        case GL_TESS_EVALUATION_SHADER:
            tese_ = std::move(program);
            break;
        case GL_GEOMETRY_SHADER:
            geom_ = std::move(program);
            break;
        case GL_FRAGMENT_SHADER:
            frag_ = std::move(program);
            break;
        case GL_COMPUTE_SHADER:
            comp_ = std::move(program);
            break;
        default:
            break;
        }
    }

    GLuint pipeline;
    glGenProgramPipelines(1, &pipeline);

    pipeline_ = std::shared_ptr<GLuint>(new GLuint(pipeline), [](GLuint *id) {
        glDeleteProgramPipelines(1, id);
        delete id;
    });
}

const glc::Program &SeparableProgramWrapper::vert_program() const
{
    return vert_;
}

const glc::Program &SeparableProgramWrapper::tesc_program() const
{
    return tesc_;
}

const glc::Program &SeparableProgramWrapper::tese_program() const
{
    return tese_;
}

const glc::Program &SeparableProgramWrapper::geom_program() const
{
    return geom_;
}

const glc::Program &SeparableProgramWrapper::frag_program() const
{
    return frag_;
}

const glc::Program &SeparableProgramWrapper::comp_program() const
{
    return comp_;
}

GLuint SeparableProgramWrapper::get_id() const
{
    return *pipeline_;
}

} // namespace detail

SeparableProgram create_separable_program(const std::vector<std::string> &shader_filenames)
{
    return std::make_shared<detail::SeparableProgramWrapper>(shader_filenames);
}

} // namespace glc
