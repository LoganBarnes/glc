// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#include <glc/VertexArray.hpp>

namespace glc {

namespace detail {

void VertexArrayWrapper::bind() const
{
    glBindVertexArray(get_id());
}

void VertexArrayWrapper::unbind() const
{
    glBindVertexArray(0);
}

GLuint VertexArrayWrapper::get_id() const
{
    return *vao_;
}

/**
 * @warning assumes a VBO is bound
 */
void VertexArrayWrapper::set_attributes(GLuint program_id,
                                        GLsizei total_stride,
                                        const std::vector<VAOElement> &elements)
{
    bind();

    for (const auto &vao_elmt : elements) {
        int pos = glGetAttribLocation(program_id, vao_elmt.name.c_str());
        if (pos < 0) {
            std::stringstream msg;
            msg << "attrib location " << vao_elmt.name << " not found for program id " << program_id;

            throw std::runtime_error(msg.str());
        }

        auto position = static_cast<GLuint>(pos);

        glEnableVertexAttribArray(position);
        switch (vao_elmt.type) {
        case GL_BYTE:
        case GL_UNSIGNED_BYTE:
        case GL_SHORT:
        case GL_UNSIGNED_SHORT:
        case GL_INT:
        case GL_UNSIGNED_INT:
            glVertexAttribIPointer(position,
                                   vao_elmt.size, // Num coordinates per position
                                   vao_elmt.type, // Type
                                   total_stride, // Stride, 0 = tightly packed
                                   vao_elmt.ptr_offset // Array buffer offset
            );
            break;
        case GL_DOUBLE:
            glVertexAttribLPointer(position,
                                   vao_elmt.size, // Num coordinates per position
                                   vao_elmt.type, // Type
                                   total_stride, // Stride, 0 = tightly packed
                                   vao_elmt.ptr_offset // Array buffer offset
            );
            break;
        default:
            glVertexAttribPointer(position,
                                  vao_elmt.size, // Num coordinates per position
                                  vao_elmt.type, // Type
                                  GL_FALSE, // Normalized
                                  total_stride, // Stride, 0 = tightly packed
                                  vao_elmt.ptr_offset // Array buffer offset
            );
            break;
        }
    }

    unbind();
}

} // namespace detail

} // namespace glc
