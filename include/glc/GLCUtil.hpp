// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>

namespace glc {

/**
 * @return 0 if type is not a common GL type
 */
template <typename T>
GLenum gl_type()
{
    if (std::is_same<T, GLboolean>::value) {
        return GL_BOOL;
    }
    if (std::is_same<T, GLbyte>::value) {
        return GL_BYTE;
    }
    if (std::is_same<T, GLshort>::value) {
        return GL_SHORT;
    }
    if (std::is_same<T, GLint>::value) {
        return GL_INT;
    }
    if (std::is_same<T, GLubyte>::value) {
        return GL_UNSIGNED_BYTE;
    }
    if (std::is_same<T, GLushort>::value) {
        return GL_UNSIGNED_SHORT;
    }
    if (std::is_same<T, GLuint>::value) {
        return GL_UNSIGNED_INT;
    }
    if (std::is_same<T, GLfloat>::value) {
        return GL_FLOAT;
    }
    if (std::is_same<T, GLdouble>::value) {
        return GL_DOUBLE;
    }
    return 0;
}

/**
 * Enables:
 *     GL_DEPTH_TEST
 *     GL_PRIMITIVE_RESTART
 *     GL_PROGRAM_POINT_SIZE
 *     GL_POLYGON_OFFSET_LINE
 *     GL_CULL_FACE
 *
 * Sets:
 *     PolygonOffset to (-1, -1)
 *     CullFace to GL_BACK
 *     FrontFace to GL_CC
 *     ClearColor to (0, 0, 0, 1)
 */
void set_common_defaults();

} // namespace glc
