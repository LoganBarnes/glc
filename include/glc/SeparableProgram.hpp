// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/Program.hpp>

namespace glc {

struct SeparableShader
{
    enum Type : unsigned
    {
        VERTEX = 0b0000001,
        TESS_CONTROL = 0b0000010,
        TESS_EVAL = 0b0000100,
        GEOMETRY = 0b0001000,
        FRAGMENT = 0b0010000,
        COMPUTE = 0b0100000,
        ALL = 0b0111111,
    };
};

namespace detail {

class SeparableProgramWrapper
{
public:
    explicit SeparableProgramWrapper(const std::vector<std::string> &shader_filenames);

    template <typename UsageFunc>
    void use(const UsageFunc &usage_func, unsigned shader_mask = SeparableShader::ALL) const;

    const glc::Program &vert_program() const;
    const glc::Program &tesc_program() const;
    const glc::Program &tese_program() const;
    const glc::Program &geom_program() const;
    const glc::Program &frag_program() const;
    const glc::Program &comp_program() const;

    GLuint get_id() const;

private:
    std::shared_ptr<GLuint> pipeline_;
    glc::Program vert_;
    glc::Program tesc_;
    glc::Program tese_;
    glc::Program geom_;
    glc::Program frag_;
    glc::Program comp_;
};

template <typename UsageFunc>
void SeparableProgramWrapper::use(const UsageFunc &usage_func, unsigned shader_mask) const
{
    // get the program id for each enabled and available shader
    GLuint program_ids[6] = {
        (vert_program() && (shader_mask & SeparableShader::VERTEX)) ? vert_program()->get_id() : 0,
        (tesc_program() && (shader_mask & SeparableShader::TESS_CONTROL)) ? tesc_program()->get_id() : 0,
        (tese_program() && (shader_mask & SeparableShader::TESS_EVAL)) ? tese_program()->get_id() : 0,
        (geom_program() && (shader_mask & SeparableShader::GEOMETRY)) ? geom_program()->get_id() : 0,
        (frag_program() && (shader_mask & SeparableShader::FRAGMENT)) ? frag_program()->get_id() : 0,
        (comp_program() && (shader_mask & SeparableShader::COMPUTE)) ? comp_program()->get_id() : 0,
    };
    glUseProgram(0);
    glUseProgramStages(get_id(), GL_VERTEX_SHADER_BIT, program_ids[0]);
    glUseProgramStages(get_id(), GL_TESS_CONTROL_SHADER_BIT, program_ids[1]);
    glUseProgramStages(get_id(), GL_TESS_EVALUATION_SHADER_BIT, program_ids[2]);
    glUseProgramStages(get_id(), GL_GEOMETRY_SHADER_BIT, program_ids[3]);
    glUseProgramStages(get_id(), GL_FRAGMENT_SHADER_BIT, program_ids[4]);
    glUseProgramStages(get_id(), GL_COMPUTE_SHADER_BIT, program_ids[5]);
    glBindProgramPipeline(get_id());
    usage_func();
    glBindProgramPipeline(0);
}

} // namespace detail

SeparableProgram create_separable_program(const std::vector<std::string> &shader_filenames);

template <typename... Shaders>
SeparableProgram create_separable_program(const std::string &shader_filename, const Shaders... shader_filenames)
{
    std::vector<std::string> filenames;
    detail::append_filenames(&filenames, shader_filename, shader_filenames...);
    return create_separable_program(filenames);
}

} // namespace glc
