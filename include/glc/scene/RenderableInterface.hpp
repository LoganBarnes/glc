// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>
#include <glm/glm.hpp>
#include <utility>

namespace glc {

class StaticScene;
struct UpdatableInfo;

class RenderableInterface
{
public:
    virtual ~RenderableInterface() = default;

    virtual void on_render(const glc::Camera &camera,
                           const UpdatableInfo &info,
                           const StaticScene &scene,
                           const glm::mat4 &full_transform) const = 0;
};

} // namespace glc
