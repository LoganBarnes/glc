// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/scene/RenderableInterface.hpp>
#include <vector>
#include <string>

namespace glc {

using uid = const void *;

namespace detail {

#define ADD_CONSTANT_SCENE_TYPE(TypeName, Type)                                                                        \
    struct Constant##TypeName                                                                                          \
    {                                                                                                                  \
        const Type data;                                                                                               \
                                                                                                                       \
        Constant##TypeName(const Constant##TypeName &) = delete;                                                       \
        Constant##TypeName(Constant##TypeName &&) noexcept = delete;                                                   \
        Constant##TypeName &operator=(const Constant##TypeName &) = delete;                                            \
        Constant##TypeName &operator=(Constant##TypeName &&) noexcept = delete;                                        \
                                                                                                                       \
        inline friend Constant##TypeName TypeName(Type value);                                                         \
                                                                                                                       \
    private:                                                                                                           \
        explicit Constant##TypeName(Type value) : data(std::move(value)) {}                                            \
    };                                                                                                                 \
                                                                                                                       \
    inline Constant##TypeName TypeName(Type value = Type()) { return Constant##TypeName(std::move(value)); }

#define ADD_UPDATABLE_SCENE_TYPE(TypeName, Type)                                                                       \
    struct Updatable##TypeName                                                                                         \
    {                                                                                                                  \
        Type *data;                                                                                                    \
                                                                                                                       \
        Updatable##TypeName(const Updatable##TypeName &) = delete;                                                     \
        Updatable##TypeName(Updatable##TypeName &&) noexcept = delete;                                                 \
        Updatable##TypeName &operator=(const Updatable##TypeName &) = delete;                                          \
        Updatable##TypeName &operator=(Updatable##TypeName &&) noexcept = delete;                                      \
                                                                                                                       \
        inline friend Updatable##TypeName TypeName(Type *ptr);                                                         \
                                                                                                                       \
    private:                                                                                                           \
        explicit Updatable##TypeName(Type *ptr) : data(ptr) {}                                                         \
    };                                                                                                                 \
                                                                                                                       \
    inline Updatable##TypeName TypeName(Type *ptr) { return Updatable##TypeName(ptr); }

#define ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(TypeName, Type)                                                          \
    ADD_CONSTANT_SCENE_TYPE(TypeName, Type)                                                                            \
    ADD_UPDATABLE_SCENE_TYPE(TypeName, Type)

template <GLenum mode>
struct ConstantIndices
{
    const std::vector<unsigned> data;

    ConstantIndices(const ConstantIndices &) = delete;
    ConstantIndices(ConstantIndices &&) noexcept = delete;
    ConstantIndices &operator=(const ConstantIndices &) = delete;
    ConstantIndices &operator=(ConstantIndices &&) noexcept = delete;

    explicit ConstantIndices(std::vector<unsigned> value = {}) : data(std::move(value)) {}
};

} // namespace detail

using Points = detail::ConstantIndices<GL_POINTS>;
using Lines = detail::ConstantIndices<GL_LINES>;
using LineStrip = detail::ConstantIndices<GL_LINE_STRIP>;
using Triangles = detail::ConstantIndices<GL_TRIANGLES>;
using TriangleStrip = detail::ConstantIndices<GL_TRIANGLE_STRIP>;
using TriangleFan = detail::ConstantIndices<GL_TRIANGLE_FAN>;

ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(Positions, std::vector<glm::vec3>)
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(Normals, std::vector<glm::vec3>)
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(TexCoords, std::vector<glm::vec2>)
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(VertexColors, std::vector<glm::vec3>)

ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(Visible, bool)
ADD_CONSTANT_SCENE_TYPE(ChildrenVisible, bool)

ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(DisplayType, glc::DisplayMode)
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(ReadableID, std::string)
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(Parent, uid)
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(Transform, glm::mat4)
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(TransBefore, glm::mat4) // Applies the transform before the curent one
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(TransAfter, glm::mat4) // Applies the transform after the curent one
ADD_CONSTANT_AND_UPDATABLE_SCENE_TYPE(GlobalColor, glm::vec3)

struct Renderable
{
    std::shared_ptr<RenderableInterface> data;

    // constructor to take ownership of renderable class
    template <typename T, typename = std::enable_if_t<std::is_base_of<RenderableInterface, T>::value>>
    explicit Renderable(T d) : Renderable(std::make_shared<T>(std::move(d)))
    {}

    explicit Renderable(std::shared_ptr<RenderableInterface> d) : data(std::move(d)) {}
};

} // namespace glc
