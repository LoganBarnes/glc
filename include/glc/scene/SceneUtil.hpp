// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/scene/SceneTypes.hpp>

namespace glc {

class StaticScene;

class SceneUtil
{
public:
    /**
     * @brief Adds 3D arrow primitives aligned with the X (red), Y (green), and Z (blue) axes.
     */
    static uid add_axes_at_origin(StaticScene &scene, float scale = 1.f);
};

} // namespace glc
