// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/scene/SceneTypes.hpp>
#include <unordered_set>
#include <unordered_map>
#include <random>
#include <iostream>

namespace glc {

struct UpdatableInfo
{
    std::string readable_id = "An item";
    GLenum gl_draw_mode = GL_POINTS;
    glc::DisplayMode shader_display_mode = glc::DisplayMode::COLOR;
    glm::mat4 transform = glm::mat4(1.f); // change this to geom::Transform if we ever support scaling
    glm::vec3 color = {1.0f, 0.9f, 0.7f};
    bool visible = true;
    float opacity = 1.f;
    uid parent = nullptr;

    UpdatableInfo() = default;
};

namespace detail {

struct VAOData
{
    std::string name;
    int element_size;
    std::size_t vector_size;

    VAOData(std::string n, int e, std::size_t v) : name(std::move(n)), element_size(e), vector_size(v) {}
};

struct ParamType
{
    enum Type : unsigned
    {
        NONE = 0x0,
        POSITIONS = 0x1,
        NORMALS = 0x2,
        TEX_COORDS = 0x4,
        VERTEX_COLORS = 0x8,
        INDICES = 0x10,
        DISPLAY_MODE = 0x20,
        TRANSFORM = 0x40,
        PARENT = 0x80,
        READABLE_ID = 0x100,
        VISIBLE = 0x200,
        CHILDREN_VISIBLE = 0x400,
        GLOBAL_COLOR = 0x800,
        RENDERABLE = 0x1000,
    };
};

void check_error_and_update_flags(unsigned &value, ParamType::Type flag);

struct ItemData
{
    unsigned added_fields = ParamType::NONE;
    std::vector<char> vertex_data = {};
    std::vector<unsigned> index_data = {};
    std::vector<VAOData> vao_elements = {};
    std::shared_ptr<RenderableInterface> renderable = nullptr;

    UpdatableInfo info;

    ItemData() = default;
};

struct Item
{
    glc::Pipeline<char> pl = {};
    std::shared_ptr<RenderableInterface> renderable = nullptr;
    UpdatableInfo info;
    std::unordered_set<uid> children = {};

    Item() = default;
};

} // namespace detail

class StaticScene
{
public:
    explicit StaticScene();

    /**
     * @brief Render all visible items in the scene to the currently bound framebuffer
     */
    void render(const glc::Camera &camera) const;

    /**
     * @brief Adds a new geometry instance to the scene
     *        All arguments must be wrapped by a type from "glc/scene/scene_types.h".
     *        A 'glc::Positions()' or 'glc::Renderable' argument must be present in
     *        order to add geometry to the scene.
     *
     *        Example usage can be found in "glc/src/examples/scene".
     *
     * @return the unique id corresponding the the added item
     */
    template <typename... Data>
    uid add_item(Data... data);

    /**
     * @brief Updates the item with the specified uuid
     *        All arguments must be wrapped by a type from "glc/scene/scene_types.h".
     *        Updates to the actual geometry are not yet supported (Positions, Normals,
     *        TexCoords, VertexColors, Indices (Points, Lines, Triangles, etc.))
     *
     *        Updating the nil uuid updates the root node of the whole scene. The only
     *        update to the root that will be noticeable however is the transform.
     *
     * @return true if the item was found and successfully updated
     */
    template <typename... Data>
    bool update_item(uid uuid, Data... data);

    /**
     * @brief Fills the various data arguments with info from the item referenced by 'uuid'
     * @return true if the item was found
     */
    template <typename... Data>
    bool get_item_info(uid uuid, Data... data);

    /**
     * @brief Removes an item from the scene and optionally all of its children
     * @return true if the item was found and successfully removed
     */
    bool remove_item(uid uuid, bool remove_children = true);

    /**
     * @brief Adds a light to the scene. The lights themselves are not yet visualized.
     */
    void add_light(glm::vec3 position, float intensity);

    // xyz is position, w is intensity
    const std::vector<glm::vec4> &get_lights() const;
    const glc::Buffer<glm::vec4> &get_light_ssbo() const;

    bool has_item(uid uuid) const;

private:
    glc::Program program_;
    glc::Buffer<glm::vec4> lights_ssbo_;

    std::unordered_map<const void *, std::unique_ptr<detail::Item>> items_;
    std::vector<glm::vec4> lights_; // xyz is dir, w is intensity

    // per item helpers for `render` and `configure_gui`
    void render_item(const detail::Item &item, const glc::Camera &camera, const glm::mat4 &parent_transform) const;

    uid actually_add_item(const detail::ItemData &item_data);
    void remove_item_and_children(uid uuid);

    // individual functions for handling each argument in `add_item`
    template <GLenum draw_mode>
    void detail_add_item(detail::ItemData &item_data, const detail::ConstantIndices<draw_mode> &indices);
    void detail_add_item(detail::ItemData &item_data, const ConstantPositions &positions);
    void detail_add_item(detail::ItemData &item_data, const ConstantNormals &normals);
    void detail_add_item(detail::ItemData &item_data, const ConstantTexCoords &texCoords);
    void detail_add_item(detail::ItemData &item_data, const ConstantVertexColors &colors);
    void detail_add_item(detail::ItemData &item_data, const ConstantDisplayType &display_type);
    void detail_add_item(detail::ItemData &item_data, const ConstantTransform &transform);
    void detail_add_item(detail::ItemData &item_data, const ConstantParent &parent);
    void detail_add_item(detail::ItemData &item_data, const ConstantReadableID &readable_id);
    void detail_add_item(detail::ItemData &item_data, const ConstantVisible &visible);
    void detail_add_item(detail::ItemData &item_data, const ConstantGlobalColor &global_color);
    void detail_add_item(detail::ItemData &item_data, const Renderable &renderable);

    // individual functions for handling each argument in `update_item`
    void detail_update_item(unsigned added_fields, detail::Item &item, const ConstantDisplayType &display_type);
    void detail_update_item(unsigned added_fields, detail::Item &item, const ConstantTransform &transform);
    void detail_update_item(unsigned added_fields, detail::Item &item, const ConstantReadableID &readable_id);
    void detail_update_item(unsigned added_fields, detail::Item &item, const ConstantVisible &visible);
    void detail_update_item(unsigned added_fields, detail::Item &item, const ConstantChildrenVisible &visible);
    void detail_update_item(unsigned added_fields, detail::Item &item, const ConstantGlobalColor &global_color);

    // individual functions for handling each argument in `get_item_info`
    void detail_get_item_info(const detail::Item &item, UpdatableDisplayType &display_type);
    void detail_get_item_info(const detail::Item &item, UpdatableTransform &transform);
    void detail_get_item_info(const detail::Item &item, UpdatableReadableID &readable_id);
    void detail_get_item_info(const detail::Item &item, UpdatableVisible &visible);
    void detail_get_item_info(const detail::Item &item, UpdatableGlobalColor &global_color);
};

template <typename... Data>
uid StaticScene::add_item(Data... data)
{
    detail::ItemData item_data;
    // iterate over all arguments calling `detail_add_item` on each one.
    int dummy[] = {(detail_add_item(item_data, data), 0)...}; // Only accepts types from "scene_types.h"
    (void)dummy;
    return actually_add_item(item_data);
}

template <typename... Data>
bool StaticScene::update_item(uid uuid, Data... data)
{
    if (items_.find(uuid) == items_.end()) {
        std::cerr << "Item with uuid does not exist" << std::endl;
        return false;
    }
    auto &item = items_.at(uuid);
    unsigned added_fields = detail::ParamType::NONE;
    // iterate over all arguments calling `detail_update_item` on each one.
    int dummy[] = {(detail_update_item(added_fields, *item, data), 0)...}; // Only accepts types from "scene_types.h"
    (void)dummy;
    return true;
}

template <typename... Data>
bool StaticScene::get_item_info(uid uuid, Data... data)
{
    if (items_.find(uuid) == items_.end()) {
        std::cerr << "Item with id does not exist" << std::endl;
        return false;
    }
    const auto &item = items_.at(uuid);
    // iterate over all arguments calling `detail_get_item_info` on each one.
    int dummy[] = {(detail_get_item_info(*item, data), 0)...}; // Only accepts types from "scene_types.h"
    (void)dummy;
    return true;
}

template <GLenum draw_mode>
void StaticScene::detail_add_item(detail::ItemData &item_data, const detail::ConstantIndices<draw_mode> &indices)
{
    detail::check_error_and_update_flags(item_data.added_fields, detail::ParamType::INDICES);
    item_data.index_data = indices.data;
    item_data.info.gl_draw_mode = draw_mode;
}

} // namespace glc
