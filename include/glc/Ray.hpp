// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>
#include <glm/glm.hpp>

namespace glc {
namespace detail {

template <int N, typename T>
struct ray
{
    glm::vec<N, T> origin;
    glm::vec<N, T> direction;
    glm::vec<N, T> inverse_direction;

    ray(glm::vec<N, T> orig, glm::vec<N, T> dir) : origin(orig), direction(dir), inverse_direction(T(1) / direction) {}
};

} // namespace detail

} // namespace glc
