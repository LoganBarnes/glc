// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLCUtil.hpp>
#include <vector>
#include <cassert>
#include <cstring>

namespace glc {

namespace detail {

template <typename T>
class BufferWrapper
{
public:
    BufferWrapper(const T *data, std::size_t num_elements, GLenum type, GLenum usage);

    void bind() const;
    void unbind() const;

    void resize(const T *data, std::size_t num_elements);
    void resize(const std::vector<T> &data);

    void update(std::size_t start_index, std::size_t num_elements, const T *data) const;
    void update(std::size_t start_index, const std::vector<T> &data) const;

    GLuint get_id() const;
    GLenum get_buffer_type() const;
    std::size_t get_num_elements() const;

    void get_data(T *data, std::size_t num_elements, std::size_t start_index = 0) const;
    void get_data(std::vector<T> *data, std::size_t start_index = 0) const;

private:
    std::shared_ptr<GLuint> buffer_;
    std::size_t num_elements_;
    GLenum buffer_type_;
    GLenum buffer_usage_;
};

template <typename T>
BufferWrapper<T>::BufferWrapper(const T *data, std::size_t num_elements, GLenum type, GLenum usage)
    : num_elements_(num_elements), buffer_type_(type), buffer_usage_(usage)
{
    GLuint buf;
    glGenBuffers(1, &buf);
    buffer_ = std::shared_ptr<GLuint>(new GLuint(buf), [](auto *id) {
        glDeleteBuffers(1, id);
        delete id;
    });

    resize(data, num_elements_);
}

template <typename T>
void BufferWrapper<T>::bind() const
{
    glBindBuffer(buffer_type_, get_id());
}

template <typename T>
void BufferWrapper<T>::unbind() const
{
    glBindBuffer(buffer_type_, 0);
}

template <typename T>
void BufferWrapper<T>::resize(const T *data, std::size_t num_elements)
{
    num_elements_ = num_elements;

    bind();
    glBufferData(buffer_type_, static_cast<GLsizeiptr>(num_elements_ * sizeof(T)), data, buffer_usage_);
    unbind();
}

template <typename T>
void BufferWrapper<T>::resize(const std::vector<T> &data)
{
    resize(data.data(), data.size());
}

template <typename T>
void BufferWrapper<T>::update(std::size_t start_index, std::size_t num_elements, const T *data) const
{
    assert(num_elements + start_index <= num_elements_);

    bind();
    glBufferSubData(buffer_type_,
                    static_cast<GLintptr>(start_index * sizeof(T)),
                    static_cast<GLsizeiptr>(num_elements * sizeof(T)),
                    data);
    unbind();
}

template <typename T>
void BufferWrapper<T>::update(std::size_t start_index, const std::vector<T> &data) const
{
    update(start_index, data.size(), data.data());
}

template <typename T>
GLuint BufferWrapper<T>::get_id() const
{
    return *buffer_;
}

template <typename T>
GLenum BufferWrapper<T>::get_buffer_type() const
{
    return buffer_type_;
}

template <typename T>
std::size_t BufferWrapper<T>::get_num_elements() const
{
    return num_elements_;
}

template <typename T>
void BufferWrapper<T>::get_data(T *data, std::size_t num_elements, std::size_t start_index) const
{
    assert(num_elements + start_index <= num_elements_);
    bind();
    glGetBufferSubData(buffer_type_,
                       static_cast<GLintptr>(start_index * sizeof(T)),
                       static_cast<GLsizei>(num_elements * sizeof(T)),
                       data);
    unbind();
}

template <typename T>
void BufferWrapper<T>::get_data(std::vector<T> *data, std::size_t start_index) const
{
    assert(data);
    data->resize(num_elements_ - start_index);
    get_data(data->data(), data->size(), start_index);
}

template <typename T>
void create_byte_data(std::vector<char> &byte_data_out, const std::vector<T> &data)
{
    auto byte_size = data.size() * sizeof(T);
    auto copy_start = byte_data_out.size();
    byte_data_out.resize(copy_start + byte_size);
    std::memcpy(byte_data_out.data() + copy_start, data.data(), byte_size);
}

template <typename T, typename... Vecs, typename = typename std::enable_if_t<sizeof...(Vecs) != 0>>
void create_byte_data(std::vector<char> &byte_data_out, const std::vector<T> &data, const Vecs &... vecs)
{
    create_byte_data(byte_data_out, data);
    create_byte_data(byte_data_out, vecs...);
}

} // namespace detail

template <GLenum type = GL_ARRAY_BUFFER, GLenum usage = GL_STATIC_DRAW, typename T>
Buffer<T> create_buffer(const T *data, std::size_t num_elements)
{
    return std::make_shared<detail::BufferWrapper<T>>(data, num_elements, type, usage);
}

template <GLenum type = GL_ARRAY_BUFFER, GLenum usage = GL_STATIC_DRAW, typename T>
Buffer<T> create_buffer(const std::vector<T> &data)
{
    return create_buffer<type, usage>(data.data(), data.size());
}

template <GLenum type = GL_ARRAY_BUFFER, GLenum usage = GL_STATIC_DRAW, typename T, typename... Vecs>
Buffer<char> create_deinterleaved_buffer(const std::vector<T> &data, const Vecs &... vecs)
{
    std::vector<char> byte_data;
    detail::create_byte_data(byte_data, data, vecs...);
    return create_buffer<type, usage>(byte_data.data(), byte_data.size());
}

} // namespace glc
