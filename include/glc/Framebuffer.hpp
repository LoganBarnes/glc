// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>
#include <glm/glm.hpp>
#include <memory>

namespace glc {

namespace detail {

template <int Dim>
class FramebufferWrapper
{
public:
    explicit FramebufferWrapper(glm::vec<Dim, unsigned> dim,
                                const float *array = nullptr,
                                GLint internal_format = GL_RGBA32F,
                                GLenum format = GL_RGBA,
                                GLint filter_type = GL_NEAREST,
                                GLint wrap_type = GL_REPEAT);

    void bind() const;
    void unbind() const;

    template <typename UsageFunc>
    void use(const UsageFunc &usage_func) const;

    Texture get_texture() const;
    GLuint get_id() const;

    glm::vec<Dim, unsigned> get_dimensions() const;

private:
    Texture texture_;
    std::shared_ptr<GLuint> framebuffer_;
    glm::uvec3 full_dim_;
};

template <int Dim>
template <typename UsageFunc>
void FramebufferWrapper<Dim>::use(const UsageFunc &usage_func) const
{
    bind();
    glViewport(0, 0, static_cast<GLsizei>(full_dim_.x), static_cast<GLsizei>(full_dim_.y));
    usage_func();
    unbind();
}

} // namespace detail

template <int Dim>
Framebuffer<Dim> create_framebuffer(glm::vec<Dim, unsigned> dim,
                                    const float *array = nullptr,
                                    GLint internal_format = GL_RGBA32F,
                                    GLenum format = GL_RGBA,
                                    GLint filter_type = GL_NEAREST,
                                    GLint wrap_type = GL_REPEAT);

Framebuffer<2> create_framebuffer(unsigned width,
                                  unsigned height,
                                  const float *array = nullptr,
                                  GLint internal_format = GL_RGBA32F,
                                  GLenum format = GL_RGBA,
                                  GLint filter_type = GL_NEAREST,
                                  GLint wrap_type = GL_REPEAT);

} // namespace glc
