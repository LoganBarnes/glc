// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <GL/gl3w.h>
#include <string>
#include <memory>

namespace glc {

namespace detail {

template <int N, typename T>
struct ray;

template <typename T>
class BufferWrapper;

template <int Dim>
class FramebufferWrapper;

class ProgramWrapper;
class SeparableProgramWrapper;
class TextureWrapper;
class VertexArrayWrapper;

template <typename T>
class Camera;
template <typename T>
class CameraMover;

} // namespace detail

template <int N, typename T>
using ray = detail::ray<N, T>;

using ray2 = ray<2, float>;
using ray3 = ray<3, float>;

using dray2 = ray<2, double>;
using dray3 = ray<3, double>;

using Camera = detail::Camera<float>;
using CameraMover = detail::CameraMover<float>;

template <typename T>
using Buffer = std::shared_ptr<detail::BufferWrapper<T>>;

template <int Dim>
using Framebuffer = std::shared_ptr<detail::FramebufferWrapper<Dim>>;

using Program = std::shared_ptr<detail::ProgramWrapper>;
using SeparableProgram = std::shared_ptr<detail::SeparableProgramWrapper>;
using Texture = std::shared_ptr<detail::TextureWrapper>;
using VertexArray = std::shared_ptr<detail::VertexArrayWrapper>;

template <typename VboType = float, typename IboType = unsigned>
struct Pipeline
{
    Program program{nullptr};
    Buffer<VboType> vbo{nullptr};
    Buffer<IboType> ibo{nullptr};
    VertexArray vao{nullptr};
    Texture tex{nullptr};
    Framebuffer<2> fbo{nullptr};
    int draw_size{0};
};

template <typename VboType = float, typename IboType = unsigned>
struct SeparablePipeline
{
    SeparableProgram program{nullptr};
    Buffer<VboType> vbo{nullptr};
    Buffer<IboType> ibo{nullptr};
    VertexArray vao{nullptr};
    Texture tex{nullptr};
    Framebuffer<2> fbo{nullptr};
};

struct VAOElement
{
    std::string name;
    GLint size;
    GLenum type;
    void *ptr_offset;

    explicit VAOElement(std::string name_, GLint size_, GLenum type_, void *offset_)
        : name(std::move(name_)), size(size_), type(type_), ptr_offset(offset_)
    {}
};

enum class DisplayMode
{
    POSITION,
    NORMALS,
    TEX_COORDS,
    COLOR,
    TEXTURE,
    SIMPLE_SHADING,
    ADVANCED_SHADING,
    WHITE,
};

} // namespace glc
