// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>
#include <glc/Program.hpp>
#include <glc/Buffer.hpp>
#include <vector>
#include <sstream>

namespace glc {

namespace detail {

class VertexArrayWrapper
{
public:
    template <typename T>
    explicit VertexArrayWrapper(const Program &program,
                                const Buffer<T> &vbo,
                                GLsizei total_stride,
                                const std::vector<VAOElement> &elements);

    void bind() const;
    void unbind() const;

    GLuint get_id() const;

    template <typename IboType = unsigned>
    void render(GLenum mode,
                int start,
                int num_elements,
                const Buffer<IboType> &ibo = nullptr,
                GLenum ibo_type = GL_UNSIGNED_INT);

private:
    std::shared_ptr<GLuint> vao_;

    /**
     * @warning assumes a VBO is bound
     */
    void set_attributes(GLuint program_id, GLsizei total_stride, const std::vector<VAOElement> &elements);
};

template <typename T>
VertexArrayWrapper::VertexArrayWrapper(const Program &program,
                                       const Buffer<T> &vbo,
                                       const GLsizei total_stride,
                                       const std::vector<VAOElement> &elements)
{
    GLuint vao;
    glGenVertexArrays(1, &vao);
    vao_ = std::shared_ptr<GLuint>(new GLuint(vao), [](auto *id) {
        glDeleteVertexArrays(1, id);
        delete id;
    });

    vbo->bind();
    set_attributes(program->get_id(), total_stride, elements);
    vbo->unbind();
}

template <typename IboType>
void VertexArrayWrapper::render(GLenum mode, int start, int num_elements, const Buffer<IboType> &ibo, GLenum ibo_type)
{
    bind();

    if (ibo) {
        assert(ibo_type == GL_UNSIGNED_BYTE || ibo_type == GL_UNSIGNED_SHORT || ibo_type == GL_UNSIGNED_INT);

        ibo->bind();
        glDrawElements(mode,
                       num_elements,
                       ibo_type,
                       reinterpret_cast<void *>(static_cast<std::size_t>(start) * sizeof(IboType)));
        ibo->unbind();
    } else {
        glDrawArrays(mode, start, num_elements);
    }

    unbind();
}

} // namespace detail

template <typename T>
VertexArray create_vertex_array(const Program &program,
                                const Buffer<T> &vbo,
                                GLsizei total_stride,
                                const std::vector<VAOElement> &elements)
{
    return std::make_shared<detail::VertexArrayWrapper>(program, vbo, total_stride, elements);
}

} // namespace glc
