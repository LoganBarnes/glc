// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/camera/Camera.hpp>

namespace glc {
namespace detail {

/**
 * @class CameraMover
 * @brief class to manipulate a camera's position and direction
 * @tparam T the camera data type, float or double
 */
template <typename T>
class CameraMover
{

public:
    /**
     * @brief
     * @param camera - camera to manipulate
     */
    explicit CameraMover(Camera<T> cam = Camera<T>(), bool zup = false);

    /**
     * applies all the origin, pitch, and yaw changes
     */
    void update();

    void drag_with_mouse_delta(float deltaX, float deltaY);

    void scroll_with_delta(float deltaX, float deltaY);

    void pan_from_clip_pos(const glm::tvec2<T> &start_pos, const glm::tvec2<T> &end_pos);

    void add_update_move_dir(glm::ivec3 forwardRightUp);

    void set_origin(glm::tvec3<T> origin);

    /**
     * @brief Sets the pitch of the eye point above horizontal
     *
     *        This sets the pitch of the eye point, not the look direction. Increasing
     *        the pitch angle raises the camera's position causing the actual view
     *        angle to pitch downward.
     *
     * @param pitch of the eye point above horizontal
     */
    void set_pitch_degrees(T pitch);
    void set_yaw_degrees(T yaw);

    void pitch(T pitch_delta);
    void yaw(T yaw_delta);
    void zoom(T zoom_delta);

    void set_origin_offset(T offset);

    void set_update_move_distance(T distance);

    void set_zup(bool zup);

    void set_look_along_x(bool positive = true);
    void set_look_along_y(bool positive = true);
    void set_look_along_z(bool positive = true);
    void set_look_along_xyz();
    void rotate_45_deg_around_z(bool positive = true);

    T get_pitch_degrees() const;
    T get_yaw_degrees() const;
    T get_origin_offset() const;
    glm::tvec3<T> get_origin() const;

    Camera<T> camera;

private:
    glm::tvec3<T> origin_; ///< point around which to orbit
    T pitchDegrees_;
    T yawDegrees_;
    T offset_; ///< offset distance from origin
    glm::ivec3 fruMoveDir_; ///< front, right, up movement vector
    T moveAmt_;

    bool zup_;
};

} // namespace detail
} // namespace glc
