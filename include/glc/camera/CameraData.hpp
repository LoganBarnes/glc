// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glm/glm.hpp>

namespace glc {

/**
 * All the necessary data for a camera view orientation
 */
template <typename T>
struct ViewData
{
    glm::vec<3, T> eye_pos;
    glm::vec<3, T> look_vec;
    glm::vec<3, T> up_vec;
    glm::vec<3, T> right_vec;

    glm::mat<4, 4, T> view_matrix;

    T focal_dist;
};

/**
 * All the necessary data for a perspective camera projection
 */
template <typename T>
struct PerspectiveData
{
    T fovy_degrees;
    T fovy_radians;
    T aspect_ratio;
    T near_plane;
    T far_plane;

    glm::mat<4, 4, T> projection_matrix;
    glm::mat<4, 4, T> scale_matrix;
};

/**
 * All the necessary data for an orthographic camera projection
 */
template <typename T>
struct OrthographicData
{
    T ortho_left;
    T ortho_right;
    T ortho_bottom;
    T ortho_top;
    T ortho_near;
    T ortho_far;

    glm::mat<4, 4, T> projection_matrix;
    glm::mat<4, 4, T> scale_matrix;
};

/**
 * @brief Creates and stores the data for a camera view orientation
 * @param eye is the camera position
 * @param point is a point to look at
 * @param up is the upward orientation of the camera
 */
template <typename T>
ViewData<T> look_at(glm::vec<3, T> eye, glm::vec<3, T> point, glm::vec<3, T> up);

/**
 * @brief Creates and stores the data for a perspective camera projection
 *
 * See glc/camera/Camera.hpp for visuals
 *
 * @param fovy_degrees (field of view Y) is the vertical view angle in degrees
 * @param aspect is the aspect ratio (screen width / screen height)
 * @param near is the near clipping plane (clips objects closer than this distance)
 * @param far is the far clipping plane (clips objects farther than this distance)
 */
template <typename T>
PerspectiveData<T> perspective(T fovy_degrees, T aspect, T near, T far);

/**
 * @brief Creates and stores the data for an orthographic camera projection
 *
 * See glc/camera/Camera.hpp for visuals
 *
 * @param left is the distance to the left edge of the ortho projection
 * @param right is the distance to the right edge of the ortho projection
 * @param bottom is the distance to the bottom edge of the ortho projection
 * @param top is the distance to the top edge of the ortho projection
 * @param near is the near clipping distance
 * @param far is the far clipping distance
 */
template <typename T>
OrthographicData<T> ortho(T left, T right, T bottom, T top, T near, T far);

} // namespace glc
