// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>
#include <glc/camera/CameraData.hpp>

namespace glc {
namespace detail {

template <typename T>
class Camera
{
public:
    /**
     * @param use_perspective sets this camera to use a perspective projection by default
     * @param link_projection_matrices causes the perspective and orthographic matrices to
     *                                 attempt to maintain the same general view volume
     */
    explicit Camera(bool use_perspective = true, bool link_projection_matrices = true);

    /**
     * @brief Updates the appropriate camera properties when the screen size changes
     * @param width is the new screen width
     * @param height is the new screen height
     */
    void handle_screen_change(int width, int height);

    /**
     * @brief Orients the camera to look at the given point.
     * @param eye is the camera position
     * @param point is a point to look at
     * @param up is the upward orientation of the camera
     */
    void look_at(glm::vec<3, T> eye, glm::vec<3, T> point, glm::vec<3, T> up = glm::vec<3, T>(0, 1, 0));

    /**
     * @brief Sets the parameters for a perspective projection
     *
     *
     *             ---------------------                     Width
     *             |\                 /|    Aspect Ratio = ---------
     *             | \               / |                     Height
     *        ---  |  ---------------  |
     *         |   |  |             |  |
     *         |   |  |             |  |                  up
     *  Height |   |  |             |  |                  |\
     *         |   |  |             |  |                  |   look
     *         |   --_|             |_--     --- Far      |   /|
     *        ---     ---------------     --- Near        |  /
     *                                                    | /
     *                |-------------|                    eye------> right
     *                     Width
     *
     *
     * ===================================================================
     *
     *                          __---|        up
     *                     __---     |        |\
     *                __---          |        |
     *           __---    |          |        |
     *       _---  \      |          |        |
     *  eye *-__    )Fov  |          |        *-------> look
     *          ---/_     |          |
     *               ---__|          |
     *                    ---__      |
     *                         ---__ |         SIDE VIEW
     *                              --
     *
     *                    |----------|
     *                   Near       Far
     *
     * @param fovy_degrees (field of view Y) is the vertical view angle in degrees
     * @param aspect is the aspect ratio (screen width / screen height)
     * @param near is the near clipping plane (clips objects closer than this distance)
     * @param far is the far clipping plane (clips objects farther than this distance)
     */
    void perspective(T fovy_degrees, T aspect, T near, T far);

    /**
     * @brief Sets the bounds of the orthographic projection in world space
     *
     *                 ---------------
     *                /|            /|
     *               / |           / |
     *              /  |          /  |
     *    Top ---  ---------------   |
     *         |   |             |   |
     *         |   |             |----    --- Far     up
     *         |   |             |  /     /            |\
     *         |   |             | /     /             |   look
     *         |   |             |/     /              |   /|
     * Bottom ---  ---------------    --- Near         |  /
     *                                                 | /
     *             |-------------|                    eye------> right
     *           Left          Right
     *
     * @param left is the distance to the left edge of the ortho projection
     * @param right is the distance to the right edge of the ortho projection
     * @param bottom is the distance to the bottom edge of the ortho projection
     * @param top is the distance to the top edge of the ortho projection
     * @param near is the near clipping distance
     * @param far is the far clipping distance
     */
    void ortho(T left, T right, T bottom, T top, T near, T far);
    void ortho(T left, T right, T bottom, T top);

    /// see look_at() for details
    void set_eye_pos(const glm::vec<3, T> &eye);
    void set_look_vec(const glm::vec<3, T> &look); // the direction the camera is facing
    void set_up_vec(const glm::vec<3, T> &up);

    /// see perspective() for details
    void set_fovy_degrees(T fovy_degrees);
    void set_aspect_ratio(T aspect_ratio);
    void set_near_plane_distance(T near);
    void set_far_plane_distance(T far);

    /// see ortho() for details
    void set_ortho_left(T left);
    void set_ortho_right(T right);
    void set_ortho_bottom(T bottom);
    void set_ortho_top(T top);
    void set_ortho_near(T near);
    void set_ortho_far(T far);

    void set_use_perspective(bool use_perspective);
    void set_link_projection_matrices(bool link_projection_matrices);

    /// These matrices will be perspective if use_perspective_ is true and orthographic otherwise.
    const glm::mat<4, 4, T> &get_clip_from_view_matrix() const;
    const glm::mat<4, 4, T> &get_clip_from_world_matrix() const;
    const glm::mat<4, 4, T> &get_world_from_clip_matrix() const;

    const glm::mat<4, 4, T> &get_perspective_clip_from_view_matrix() const;
    const glm::mat<4, 4, T> &get_perspective_clip_from_world_matrix() const;
    const glm::mat<4, 4, T> &get_perspective_world_from_clip_matrix() const;

    const glm::mat<4, 4, T> &get_orthographic_clip_from_view_matrix() const;
    const glm::mat<4, 4, T> &get_orthographic_clip_from_world_matrix() const;
    const glm::mat<4, 4, T> &get_orthographic_world_from_clip_matrix() const;

    const glm::mat<4, 4, T> &get_view_from_world_matrix() const;

    const glm::vec<3, T> &get_eye_pos() const;
    const glm::vec<3, T> &get_look_vec() const;
    const glm::vec<3, T> &get_up_vec() const;
    const glm::vec<3, T> &get_right_vec() const;

    const T &get_focal_dist() const;

    /// see perspective() for details
    const T &get_fovy_degrees() const;
    const T &get_fovy_radians() const;
    const T &get_aspect_ratio() const;
    const T &get_near_plane_distance() const;
    const T &get_far_plane_distance() const;

    /// see ortho() for details
    const T &get_ortho_left() const;
    const T &get_ortho_right() const;
    const T &get_ortho_bottom() const;
    const T &get_ortho_top() const;
    const T &get_ortho_near() const;
    const T &get_ortho_far() const;

    bool is_using_perspective() const;
    bool is_linking_projection_matrices() const;

    glc::ray<3, T> get_ray_from_clip_pos(const glm::vec<2, T> &clip_pos) const;
    glc::ray<3, T> get_ray_from_clip_pos(T x, T y) const;

private:
    glc::ViewData<T> view_data_;
    glc::PerspectiveData<T> pers_cam_;
    glc::OrthographicData<T> orth_cam_;

    glm::mat<4, 4, T> perspective_projection_view_matrix_;
    glm::mat<4, 4, T> perspective_inverse_scale_view_matrix_;

    glm::mat<4, 4, T> orthographic_projection_view_matrix_;
    glm::mat<4, 4, T> orthographic_inverse_scale_view_matrix_;

    /// use perspective projection by default
    bool use_perspective_;
    /// perspective and orthographic matrices attempt to maintain the same general view volume
    bool link_projection_matrices_;

    void update_matrices();
    void check_link_and_update();
};

} // namespace detail
} // namespace glc
