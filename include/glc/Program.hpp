// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>
#include <glc/Buffer.hpp>
#include <vector>
#include <cassert>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace glc {

namespace detail {

class ProgramWrapper
{
public:
    explicit ProgramWrapper(const std::vector<std::string> &shader_filenames);
    explicit ProgramWrapper(const std::string &separable_shader, GLenum shader_type);

    template <typename UsageFunc>
    void use(const UsageFunc &usage_func) const;

    bool set_int_uniform_array(const std::string &uniform, const int *value, int size = 1, int count = 1) const;
    bool set_float_uniform_array(const std::string &uniform, const float *value, int size = 1, int count = 1) const;
    bool set_matrix_uniform_array(const std::string &uniform, const float *value, int size = 4, int count = 1) const;

    bool set_uniform(const std::string &uniform, bool value) const;
    bool set_uniform(const std::string &uniform, int val) const;
    bool set_uniform(const std::string &uniform, float val) const;
    bool set_uniform(const std::string &uniform, const Texture &texture, int active_tex = 0) const;

    template <int N>
    bool set_uniform(const std::string &uniform, const glm::vec<N, int> &vec) const;

    template <int N>
    bool set_uniform(const std::string &uniform, const glm::vec<N, float> &vec) const;

    template <int N>
    bool set_uniform(const std::string &uniform, const glm::mat<N, N, float> &mat) const;

    template <typename T>
    bool set_ssbo_uniform(const std::string &uniform, const glc::Buffer<T> &ssbo, GLuint binding) const;

    GLuint get_id() const;

private:
    std::shared_ptr<GLuint> program_;
};

template <typename UsageFunc>
void ProgramWrapper::use(const UsageFunc &usage_func) const
{
    glUseProgram(get_id());
    usage_func();
    glUseProgram(0);
}

template <typename T>
bool ProgramWrapper::set_ssbo_uniform(const std::string &uniform, const glc::Buffer<T> &ssbo, GLuint binding) const
{
    GLuint blockIdx = glGetProgramResourceIndex(get_id(), GL_SHADER_STORAGE_BLOCK, uniform.c_str());
    if (blockIdx == GL_INVALID_INDEX) {
        return false;
    }
    glShaderStorageBlockBinding(get_id(), blockIdx, binding);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo->get_id());
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, ssbo->get_id());
    glBindBufferRange(GL_SHADER_STORAGE_BUFFER,
                      binding,
                      ssbo->get_id(),
                      0,
                      static_cast<GLintptr>(ssbo->get_num_elements() * sizeof(T)));

    return true;
}

template <int N>
bool ProgramWrapper::set_uniform(const std::string &uniform, const glm::vec<N, int> &vec) const
{
    return set_int_uniform_array(uniform, glm::value_ptr(vec), N);
}

template <int N>
bool ProgramWrapper::set_uniform(const std::string &uniform, const glm::vec<N, float> &vec) const
{
    return set_float_uniform_array(uniform, glm::value_ptr(vec), N);
}

template <int N>
bool ProgramWrapper::set_uniform(const std::string &uniform, const glm::mat<N, N, float> &mat) const
{
    return set_matrix_uniform_array(uniform, glm::value_ptr(mat), N);
}

template <typename... Shaders>
void append_filenames(std::vector<std::string> *filename_list, const std::string &filename)
{
    assert(filename_list);
    filename_list->emplace_back(filename);
}

template <typename... Shaders, typename = std::enable_if_t<sizeof...(Shaders) != 0>>
void append_filenames(std::vector<std::string> *filename_list, const std::string &filename, const Shaders... filenames)
{
    assert(filename_list);
    filename_list->emplace_back(filename);
    append_filenames(filename_list, filenames...);
}

} // namespace detail

Program create_program(const std::vector<std::string> &shader_filenames);

template <typename... Shaders>
Program create_program(const std::string &shader_filename, const Shaders... shader_filenames)
{
    std::vector<std::string> filenames;
    detail::append_filenames(&filenames, shader_filename, shader_filenames...);
    return create_program(filenames);
}

} // namespace glc
