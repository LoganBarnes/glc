// ////////////////////////////////////////////////////////////
// Graphics Library Classes
// Copyright (c) 2018. All rights reserved.
// ////////////////////////////////////////////////////////////
#pragma once

#include <glc/GLC.hpp>
#include <glm/glm.hpp>

namespace glc {

namespace detail {

class TextureWrapper
{
public:
    TextureWrapper(const glm::uvec3 &dim,
                   GLenum tex_type,
                   const float *array,
                   GLint internal_format,
                   GLenum format,
                   GLint filter_type,
                   GLint wrap_type);

    void bind() const;

    void set_filter_type(GLint filter_type);
    void set_wrap_type(GLint wrap_type);

    GLuint get_id() const;

private:
    std::shared_ptr<GLuint> texture_;
    GLenum tex_type_;
    GLint internal_format_;
    GLenum format_;
    GLint filter_type_;
    GLint wrap_type_;
};

} // namespace detail

Texture create_texture(unsigned width,
                       unsigned height,
                       const float *array = nullptr,
                       GLint internal_format = GL_RGBA32F,
                       GLenum format = GL_RGBA,
                       GLint filter_type = GL_NEAREST,
                       GLint wrap_type = GL_REPEAT,
                       GLenum tex_type = GL_TEXTURE_2D);

Texture create_texture(const glm::uvec2 &size,
                       const float *array = nullptr,
                       GLint internal_format = GL_RGBA32F,
                       GLenum format = GL_RGBA,
                       GLint filter_type = GL_NEAREST,
                       GLint wrap_type = GL_REPEAT,
                       GLenum tex_type = GL_TEXTURE_2D);

Texture create_texture(const glm::uvec3 &dim,
                       GLenum tex_type,
                       const float *array = nullptr,
                       GLint internal_format = GL_RGBA32F,
                       GLenum format = GL_RGBA,
                       GLint filter_type = GL_NEAREST,
                       GLint wrap_type = GL_REPEAT);

} // namespace glc
