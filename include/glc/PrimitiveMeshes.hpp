// ///////////////////////////////////////////////////////////////////////////////////////
//                                                                           |________|
//  Copyright (c) 2018 CloudNC Ltd - All Rights Reserved                        |  |
//                                                                              |__|
//        ____                                                                .  ||
//       / __ \                                                               .`~||$$$$
//      | /  \ \         /$$$$$$  /$$                           /$$ /$$   /$$  /$$$$$$$
//      \ \ \ \ \       /$$__  $$| $$                          | $$| $$$ | $$ /$$__  $$
//    / / /  \ \ \     | $$  \__/| $$  /$$$$$$  /$$   /$$  /$$$$$$$| $$$$| $$| $$  \__/
//   / / /    \ \__    | $$      | $$ /$$__  $$| $$  | $$ /$$__  $$| $$ $$ $$| $$
//  / / /      \__ \   | $$      | $$| $$  \ $$| $$  | $$| $$  | $$| $$  $$$$| $$
// | | / ________ \ \  | $$    $$| $$| $$  | $$| $$  | $$| $$  | $$| $$\  $$$| $$    $$
//  \ \_/ ________/ /  |  $$$$$$/| $$|  $$$$$$/|  $$$$$$/|  $$$$$$$| $$ \  $$|  $$$$$$/
//   \___/ ________/    \______/ |__/ \______/  \______/  \_______/|__/  \__/ \______/
//
// ///////////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <vector>
#include <glm/glm.hpp>

namespace glc {

/**
 * @brief Builds a unit cube centered at the origin.
 *        Min point: (-0.5, -0.5, -0.5)
 *        Max point: (0.5, 0.5, 0.5)
 */
void build_cube_mesh(std::vector<unsigned> *triangles,
                     std::vector<glm::vec3> *vertices,
                     std::vector<glm::vec3> *normals = nullptr,
                     std::vector<glm::vec2> *tex_coords = nullptr);

/**
 * @brief Builds a cone centered at the origin. The point is at (0, 0, 0.5)
 *        and the base is centered at (0, 0, -0.5) with a diameter of 1.
 */
void build_cone_mesh(unsigned divisions,
                     std::vector<unsigned> *triangles,
                     std::vector<glm::vec3> *vertices,
                     std::vector<glm::vec3> *normals = nullptr,
                     std::vector<glm::vec2> *tex_coords = nullptr);

/**
 * @brief Builds a cylinder centered at the origin. The lengthwise axis lies
 *        along the Z-axis. Its length and diameter are both 1.
 */
void build_cylinder_mesh(unsigned divisions,
                         std::vector<unsigned> *triangles,
                         std::vector<glm::vec3> *vertices,
                         std::vector<glm::vec3> *normals = nullptr,
                         std::vector<glm::vec2> *tex_coords = nullptr);

/**
 * @brief Builds a sphere centered at the origin with a diameter of 1.
 */
void build_sphere_mesh(unsigned divisions,
                       std::vector<unsigned> *triangles,
                       std::vector<glm::vec3> *vertices,
                       std::vector<glm::vec3> *normals = nullptr,
                       std::vector<glm::vec2> *tex_coords = nullptr);

/**
 * @brief Builds a torus at the origin centered around the Z-axis
 */
void build_torus_mesh(unsigned divisions,
                      float max_radius, ///< largest radius from center point to outer edge
                      float inner_radius, ///< radius of the "tube"
                      std::vector<unsigned> *triangles,
                      std::vector<glm::vec3> *vertices,
                      std::vector<glm::vec3> *normals = nullptr,
                      std::vector<glm::vec2> *tex_coords = nullptr);

} // namespace glc
